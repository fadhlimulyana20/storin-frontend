/** @type {import('next').NextConfig} */
const path = require('path')

const nextConfig = {
  reactStrictMode: true,
  sassOptions: [path.join(__dirname, 'styles')],
  images: {
    domains: ['is3.cloudhost.id'],
  },
  // distDir: 'build',
}

module.exports = nextConfig
