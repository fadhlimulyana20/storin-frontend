import { faCheck, faQuestion, faTriangleExclamation } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Dialog, Transition } from "@headlessui/react"
import { Dispatch, Fragment, MouseEventHandler, ReactNode, SetStateAction, useEffect, useMemo, useRef, useState } from "react"
import Button from "../../atoms/button"

interface Props {
    id?: string
    open: boolean
    setOpen: Dispatch<SetStateAction<boolean>>
    type?: "success" | "error" | "question"
    title?: string
    children: ReactNode | any
    confirmText?: string
    cancelText?: string
    disableConfirm?: boolean
    onConfirm?: MouseEventHandler<HTMLButtonElement>
    plain?: boolean
    noConfirmButton?: boolean
}

const Modal = (
    {
        id,
        open,
        setOpen,
        type = "success",
        title = "Modal",
        children,
        confirmText="Ya",    
        cancelText="Tidak",
        disableConfirm=false,
        onConfirm=() => setOpen(false),
        plain=false,
        noConfirmButton=false
    }: Props
) => {

    const cancelButtonRef = useRef(null)

    const icon = useMemo(() => {
        if (type === "success") {
            return (
                <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                    <FontAwesomeIcon icon={faCheck} />
                </div>
            )
        } else if (type === "error") {
            return (
                <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                    <FontAwesomeIcon icon={faTriangleExclamation} />
                </div>
            )
        } else if (type === "question") {
            return (
                <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-blue-100 sm:mx-0 sm:h-10 sm:w-10">
                    <FontAwesomeIcon icon={faQuestion} />
                </div>
            )
        }
    }, [type])

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog id={id} as="div" className="fixed z-50 inset-0 overflow-y-auto" initialFocus={cancelButtonRef} onClose={setOpen}>
                <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >
                        <div className="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg w-full">
                            <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div className="sm:flex sm:items-start">
                                    {plain ? '' : icon}
                                    <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left flex-1">
                                        <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                                            {title}
                                        </Dialog.Title>
                                        <div className="mt-2">
                                            <p className="text-gray-500">
                                                {children}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse gap-x-2">
                                { noConfirmButton ? '' : (
                                    <Button
                                        variant="primary"
                                        className="py-1"
                                        onClick={onConfirm}
                                        disabled={disableConfirm}
                                    >
                                        {confirmText}
                                    </Button>
                                ) }
                                <Button
                                    variant="white"
                                    className="border py-1"
                                    onClick={() => setOpen(false)}
                                    ref={cancelButtonRef}
                                >
                                    {cancelText}
                                </Button>
                            </div>
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    )
}

export default Modal