import { faArrowLeft } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useRouter } from "next/router"
import Button from "../../atoms/button"

interface Props {
    title: string | any
}

const TitleBackButton = ({ title }: Props) => {
    const router = useRouter()

    return (
        <div className="flex gap-x-2 items-center">
        <Button
            variant="outline-primary"
            type="button"
            className="h-10 w-10"
            circle={true}
            onClick={ () => router.back() }
        >
            <FontAwesomeIcon icon={faArrowLeft} />
        </Button>
        <h1 className="text-2xl font-bold">{ title }</h1>
    </div>
    )
}

export default TitleBackButton