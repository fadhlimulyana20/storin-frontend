import { Menu } from "@headlessui/react";
import { useRouter } from "next/router";

export interface MenuListItem {
    text: string;
    href?: string | any;
    name: string;
    onClick?: () => any;
}

interface Props {
    menuList: Array<MenuListItem>;
    children: React.ReactNode;
}

function classNames(...classes: string[]) {
    return classes.filter(Boolean).join(' ')
}   


const NavDropdown = ({ menuList, children }: Props) => {
    const router = useRouter()
    return (
        <Menu as="div" className="relative inline-block text-left">
            <div>
                <Menu.Button className="inline-flex items-center justify-center w-full rounded-md px-2 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none">
                    {children}
                </Menu.Button>
            </div>
            <Menu.Items className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className="py-1">
                    {/* {withRole ? (
                        <div className="border-b">
                            <div className="px-3 mb-2 pt-1">
                                <p className="text-sm font-semibold">Mode :</p>
                            </div>
                            {globalState.auth.roles?.map((item, idx) => {
                                return (
                                    <Menu.Item key={idx}>
                                        {({ active }) => (
                                            <a
                                                onClick={() => setRole(item.id)}
                                                className={classNames(
                                                    activeRoleId == item.id ? 'bg-red-500 text-white cursor-pointer hover:bg-red-700' : 
                                                    active ? 'bg-gray-100 text-gray-900 cursor-pointer' : 'text-gray-700',
                                                    'block px-4 py-2 text-sm', 
                                                )}
                                            >
                                                {item.name}
                                            </a>
                                        )}
                                    </Menu.Item>
                                )
                            })}
                        </div>
                    ) : ''} */}
                    {menuList.map((item, idx) => {
                        return (
                            <Menu.Item key={idx}>
                                {({ active }) => {
                                    if (item.href) {
                                        return (
                                            <a
                                                onClick={() => router.push(item.href)}
                                                className={classNames(
                                                    active ? 'bg-gray-100 text-gray-900 cursor-pointer' : 'text-gray-700',
                                                    'block px-4 py-2 text-sm'
                                                )}
                                            >
                                                {item.text}
                                            </a>
                                        )
                                    } else if (item.onClick) {
                                        return (
                                            <a
                                                onClick={item.onClick}
                                                className={classNames(
                                                    active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                                    'block px-4 py-2 text-sm cursor-pointer'
                                                )}
                                            >
                                                {item.text}
                                            </a>
                                        )
                                    } else {
                                        return (
                                            <a
                                                href="#"
                                                className={classNames(
                                                    active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                                    'block px-4 py-2 text-sm'
                                                )}
                                            >
                                                {item.text}
                                            </a>
                                        )
                                    }
                                }}
                            </Menu.Item>
                        )
                    })}
                </div>
            </Menu.Items>
        </Menu>
    )
}

export default NavDropdown