import { useRouter } from "next/router"
import VerticalNavItem from "../../atoms/nav/vertical/item"

type Menu = {
    display: string | any,
    name: string,
    href: string,
    index?: boolean
}

interface Props {
    menu: Array<Menu>
}

const VerticalNavGroup = ({ menu }: Props) => {
    const router = useRouter()

    return (
        <div>
            { menu.map(m => {
                let active = false

                if (m.index) {
                    if (router.pathname === m.href) {
                        active = true
                    }
                } else {
                    if (router.pathname.includes(m.href)) {
                        active = true
                    }
                }

                return (
                    <VerticalNavItem key={m.name} href={m.href} active={active}>
                        { m.display }
                    </VerticalNavItem>
                )
            }) }
        </div>
    )
}

export default VerticalNavGroup