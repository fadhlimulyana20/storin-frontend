import Head from "next/head"
import { ReactNode } from "react"
import OpenGraph from "../../atoms/openGraph"

interface Props {
    title: string
    description: string,
    url?: string,
    children?: ReactNode
}

const Header = ({ title, description, url, children }: Props) => {
    return (
        <Head>
            <OpenGraph
                title={ title }
                description={ description }
            />
            <title>{ title }</title>
            {children}
        </Head>
    )
}

export default Header