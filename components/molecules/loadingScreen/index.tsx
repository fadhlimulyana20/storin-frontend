import { faSpinner } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Image from "next/image"

const DefaultLoadingScreen = () => {
    return (
        <div className="min-h-screen bg-white flex justify-center items-center">
            <div className="flex flex-col items-center">
                <div className="w-36 mb-5">
                    <Image src="/logo.png" layout="responsive" width={100} height={38} alt="logo-storin" />
                </div>
                <FontAwesomeIcon icon={faSpinner} className="text-2xl animate-spin text-blue-700    " />
            </div>
        </div>
    )
}

export default DefaultLoadingScreen