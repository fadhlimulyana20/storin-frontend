import Link from "next/link"
import { LegacyRef, MouseEventHandler, ReactNode, useEffect, useState } from "react"
import { Url } from "url"

interface Props {
    children: ReactNode | any
    variant?: "primary" | "light" | "white" | "outline-primary" | "outline-danger" | "outline-success"
    onClick?: MouseEventHandler<HTMLButtonElement> | undefined,
    className?: string,
    type?: "button" | "submit" | "reset",
    disabled?: boolean,
    circle?: boolean
    ref?: LegacyRef<HTMLButtonElement>
    link?: boolean
    href?: string | Url | any
    title?: string | any
}

const Button = ({ children, variant = "primary", onClick, className, type = "button", disabled = false, circle = false, ref, link = false, href, title, ...props }: Props) => {
    const [classVariant, setclassVariant] = useState("")

    useEffect(() => {
        if (variant === "primary") {
            setclassVariant("bg-blue-500 text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-800 disabled:bg-blue-300")
        } else if (variant === "light") {
            setclassVariant("bg-gray-50 text-gray-800 border hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-gray-700 disabled:bg-gray-100")
        } else if (variant === "white") {
            setclassVariant("bg-white text-gray-800 hover:bg-blue-100 focus:outline-none focus:ring-2 focus:ring-blue-200 disabled:text-gray-500")
        } else if (variant === "outline-primary") {
            setclassVariant("bg-white border border-blue-500 text-blue-700 hover:bg-blue-600 hover:text-white focus:outline-none focus:ring-2 focus:ring-blue-800 disabled:text-blue-200")
        } else if (variant === "outline-danger") {
            setclassVariant("bg-white border border-red-500 text-red-700 hover:bg-red-500 hover:text-white focus:outline-none focus:ring-2 focus:ring-red-800 disabled:text-red-200")
        } else if (variant === "outline-success") {
            setclassVariant("bg-white border border-green-500 text-green-700 hover:bg-green-500 hover:text-white focus:outline-none focus:ring-2 focus:ring-green-800 disabled:text-green-200")
        }
    }, [variant])


    if (link) {
        return (
            <Link href={href}>
                <a className={`${classVariant} px-3 ${circle ? 'rounded-full' : 'rounded'} ${className}`} type={type} >
                    {children}
                </a>
            </Link>
        )
    }

    return (
        <button className={`${classVariant} px-3 ${circle ? 'rounded-full' : 'rounded'} ${className}`} disabled={disabled} onClick={onClick} type={type} ref={ref} title={title} {...props}>
            {children}
        </button>
    )
}

export default Button