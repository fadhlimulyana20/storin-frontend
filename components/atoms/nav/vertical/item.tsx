import Link from "next/link"
import { MouseEventHandler, ReactNode } from "react"

interface Props {
    children: ReactNode | any,
    href?: string | any,
    onClick?: MouseEventHandler<any>,
    active?: boolean
}

const VerticalNavItem = ({ children, href, onClick, active}: Props) => {
    const classNav = "font-semibold rounded-lg flex items-center p-2 hover:bg-blue-200 hover:text-blue-900 group cursor-pointer"
    const normalclassNav = "text-base text-blue-500"
    const activeClassNav = "bg-blue-500 text-white"

    if (onClick) {
        return (
            <li>
                <button className={`${classNav} ${active ? activeClassNav : normalclassNav}`} onClick={onClick}>
                    {children}
                </button>
            </li>
        )
    }

    if (href) {
        return (
           <li>
                <Link href={href}>
                    <a className={`${classNav} ${active ? activeClassNav : normalclassNav}`}>
                        { children }
                    </a>
                </Link>
           </li> 
        )
    }

    return (
        <li>
            <a className={`${classNav} ${active ? activeClassNav : normalclassNav}`}>
                { children }
            </a>
        </li> 
     )
}

export default VerticalNavItem