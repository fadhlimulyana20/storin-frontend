import { ReactNode, useEffect, useState } from "react"

interface Props {
    children: ReactNode | any,
    className?: string,
    size?: "wide" | "narrow"
}

const Container = ({ children, className, size="narrow" }: Props) => {
    const [classSize, setClassSize] = useState("")

    useEffect(() => {
        if (size === "wide") {
            setClassSize("lg:px-10")
        } else if (size === "narrow") {
            setClassSize("lg:px-32")
        }
    }, [size])

    return (
        <div className={`${ classSize } md:px-10 px-4 ${className}`}>
            { children }
        </div>
    )
}

export default Container