import Link from "next/link"
import { ReactNode } from "react"

interface Props {
    href: string
    children?: ReactNode | any
}

const Slink = ({ href, children }: Props) => {
    return (
        <Link href={href}>
            <a className="text-blue-500 hover:text-blue-800">
                { children }
            </a>
        </Link>
    )
}

export default Slink