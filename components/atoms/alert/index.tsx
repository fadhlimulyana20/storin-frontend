import { ReactNode, useEffect, useState } from "react"

interface Props {
    children: ReactNode | any
    variant?: "success" | "error"  
    className?: string,
    show?: boolean
}

const Alert = ({ children, variant="success", className, show=true }: Props) => {
    const [classVariant, setClassVariant] = useState('') 

    useEffect(() => {
        if (variant === "success") {
            setClassVariant('bg-green-200 text-green-800')
        } else if (variant === "error") {
            setClassVariant('bg-red-200 text-red-800')
        }
    }, [variant])

    return (
        <div className={`rounded p-3 ${classVariant} ${className} ${ show ? '' : 'hidden' }`}>
            {children}
        </div>
    )
}

export default Alert