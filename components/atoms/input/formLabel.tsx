import { ReactNode } from "react"

interface Props {
    children: ReactNode | any
    htmlFor?: string
}

const FormLabel = ({ children, htmlFor }: Props) => {
    return (
        <label 
            htmlFor={htmlFor} 
            className="text-base pl-1 text-gray-700"
        >
            { children }
        </label>
    )
}

export default FormLabel