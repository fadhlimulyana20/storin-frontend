import { ChangeEventHandler, FocusEventHandler, HTMLInputTypeAttribute } from "react"

interface Props {
    placeholder?: string
    id?: string
    name?: string
    value?: string | number | readonly string[]
    onChange?: ChangeEventHandler<HTMLTextAreaElement>
    onBlur?: FocusEventHandler<HTMLTextAreaElement>
    rows?: number
}

const TextArea = ({ placeholder, id, name, value, onChange, onBlur, rows }: Props) => {
    return (
        <textarea
            className="bg-sky-50 focus:bg-white px-2 py-2 border border-gray-200 focus:border-blue-700 rounded focus:outline-none focus:border-blue-500 focus:ring-1 w-full" 
            placeholder={ placeholder } 
            id={id}
            name={name}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            rows={rows}
        />
    )
}

export default TextArea