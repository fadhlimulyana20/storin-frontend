import { ChangeEventHandler, FocusEventHandler, HTMLInputTypeAttribute } from "react"

interface Props {
    type: HTMLInputTypeAttribute
    placeholder?: string
    id?: string
    name?: string
    value?: string | number | readonly string[]
    onChange?: ChangeEventHandler<HTMLInputElement>
    onBlur?: FocusEventHandler<HTMLInputElement>
    full?: boolean
    size?: number
    className?: string
}

const FormInput = ({ className, type, placeholder, id, name, value, onChange, onBlur, full=true , size }: Props) => {
    return (
        <input 
            className={`bg-sky-50 focus:bg-white px-2 py-2 border border-gray-200 focus:border-blue-700 rounded focus:outline-none focus:border-blue-500 focus:ring-1 ${full ? 'w-full' : ''} ${className}`} 
            type={ type } 
            placeholder={ placeholder } 
            id={id}
            name={name}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            size={size}
        />
    )
}

export default FormInput