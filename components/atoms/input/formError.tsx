import { ReactNode } from "react"

interface Props {
    children: ReactNode | string | any
}

const FormError = ({ children }: Props) => {
    return (
        <p className="text-sm text-red-500 ml-1">
            {children}
        </p>
    )
}

export default FormError