import { ReactNode } from "react"

interface TableProps {
    children: ReactNode
    withPagination?: boolean
}

export const Table = ({ children, withPagination=false, ...props }: TableProps) => {
    return (
        <div className="overflow-x-auto rounded-lg shadow">
            <table className="w-full whitespace-nowrap border bg-white" {...props}>
                {children}
            </table>
            { withPagination && (
                <div className="py-2 px-3 min-w-full bg-white">
                    <div className="flex-1 flex justify-between sm:hidden">
                        <a
                            href="#"
                            className="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                        >
                            Previous
                        </a>
                        <a
                            href="#"
                            className="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                        >
                            Next
                        </a>
                    </div>
                    <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                        <div>
                            <p className="text-sm text-gray-700">
                                Showing <span className="font-medium">1</span> to <span className="font-medium">10</span> of{' '}
                                <span className="font-medium">97</span> results
                            </p>
                        </div>
                        <div>
                            <nav className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
                                <a
                                    href="#"
                                    className="relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"
                                >
                                    <span className="sr-only">Previous</span>
                                    {/* <ChevronLeftIcon className="h-5 w-5" aria-hidden="true" /> */}
                                </a>
                                {/* Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" */}
                                <a
                                    href="#"
                                    aria-current="page"
                                    className="z-10 bg-indigo-50 border-indigo-500 text-indigo-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium"
                                >
                                    1
                                </a>
                                <a
                                    href="#"
                                    className="bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium"
                                >
                                    2
                                </a>
                                <a
                                    href="#"
                                    className="bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium"
                                >
                                    3
                                </a>
                                <span className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700">
                                    ...
                                </span>
                                <a
                                    href="#"
                                    className="bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium"
                                >
                                    8
                                </a>
                                <a
                                    href="#"
                                    className="bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium"
                                >
                                    9
                                </a>
                                <a
                                    href="#"
                                    className="bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium"
                                >
                                    10
                                </a>
                                <a
                                    href="#"
                                    className="relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"
                                >
                                    <span className="sr-only">Next</span>
                                    {/* <ChevronRightIcon className="h-5 w-5" aria-hidden="true" /> */}
                                </a>
                            </nav>
                        </div>
                    </div>
                </div>
            ) }
        </div>
    )
}

interface TableHeadProps {
    children: ReactNode
}

export const TableHead = ({ children, ...props }: TableHeadProps) => {
    return (
        <thead className="bg-blue-500 text-white text-left" {...props}>
            {children}
        </thead>
    )
}

interface TableHeadRowProps {
    children?: ReactNode | null | any
}

export const TableHeadRow = ({ children, ...props }: TableHeadRowProps) => {
    return (
        <tr {...props}>
            {children}
        </tr>
    )
}

interface TableHeaderCellProps {
    children?: ReactNode | null | any
}

export const TableHeaderCell = ({ children, ...props }: TableHeaderCellProps) => {
    return (
        <th className="py-3 px-3">
            {children}
        </th>
    )
}

interface TableBodyProps {
    children?: ReactNode | null | any
}

export const TableBody = ({ children, ...props }: TableBodyProps) => {
    return (
        <tbody>
            {children}
        </tbody>
    )
}


interface TableBodyRowProps {
    children?: ReactNode | null | any
    className?: string
}

export const TableBodyRow = ({ children, className, ...props }: TableBodyRowProps) => {
    return (
        <tr {...props} className={`hover:bg-blue-50 ${className}`}>
            {children}
        </tr>
    )
}


interface TableDataCellProps {
    children?: ReactNode | null | any
}

export const TableDataCell = ({ children, ...props }: TableDataCellProps) => {
    return (
        <td className="px-3 py-2 border-b">
            {children}
        </td>
    )
}