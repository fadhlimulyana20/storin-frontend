import { faChevronDown, faUser } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Image from "next/image"
import Link from "next/link"
import { useRouter } from "next/router"
import { useContext } from "react"
import { Context } from "../../../contexts/store"
import Button from "../../atoms/button"
import Container from "../../atoms/container"
import Slink from "../../atoms/links/link"
import NavDropdown, { MenuListItem } from "../../molecules/navGroup/dropdown"

const Navbar = () => {
    const router = useRouter()
    const { globalState, dispatch } = useContext(Context);

    const authMenu: Array<MenuListItem> = [
        {
            name: 'dashboard',
            text: 'Dashboard',
            onClick: () => {
                router.push('/dashboard')
            }
        },
        {
            name: 'logout',
            text: 'Log out',
            onClick: () => {
                localStorage.removeItem('token')
                dispatch({ type: "LOGOUT", payload: null })
                router.push('/')
            }
        }
    ]

    return (
        <div className="bg-white py-4 w-full shadow fixed top-0 z-50">
            <Container>
                <div className="flex items-center justify-between">
                    <div className="flex gap-10 items-center">
                        <div className="w-28">
                            <Slink href="/">
                                <Image src="/logo.png" layout="responsive" width={100} height={38} alt="logo-storin" />
                            </Slink>                    
                        </div>
                        <div className="flex gap-4 md:inline-flex hidden">
                            <a className="font-bold hover:text-blue-700 active:text-blue-700" href="https://medium.com/@storinapp">Blog</a>
                            <Link  href="/about-us">  
                                <a className="font-bold hover:text-blue-700 active:text-blue-700">Tentang</a>
                            </Link>
                        </div>
                    </div>
                    <div className="">
                        {globalState.auth.isUserAuthenticated ? (
                            <NavDropdown menuList={authMenu}>
                                <FontAwesomeIcon icon={faUser} className="mr-2" />
                                {globalState.auth.isUserAuthenticated ? globalState.auth.loggedUser.username : ''}
                                <FontAwesomeIcon icon={faChevronDown} className="text-sm ml-2" />
                            </NavDropdown>
                        ) : (
                            <div className="flex gap-x-2">
                                <Button variant="primary" onClick={() => router.push('/login')}>
                                    Masuk
                                </Button>
                                <Button variant="outline-primary" onClick={() => router.push('/register')}>
                                    Daftar
                                </Button>
                            </div>
                        )}
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default Navbar