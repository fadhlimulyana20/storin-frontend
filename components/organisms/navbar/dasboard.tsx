import { faBars, faChevronDown, faHamburger, faUser } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Image from "next/image"
import { useRouter } from "next/router"
import { useContext } from "react"
import { Context } from "../../../contexts/store"
import Button from "../../atoms/button"
import Container from "../../atoms/container"
import Slink from "../../atoms/links/link"
import NavDropdown, {MenuListItem} from "../../molecules/navGroup/dropdown"

const NavbarDashboard = () => {
    const { globalState, dispatch } = useContext(Context);
    const router = useRouter()

    const authMenu: Array<MenuListItem> = [
        {
            name: 'setting',
            text: 'Pengaturan',
            onClick: () => {
                router.push('/dashboard/settings')
            }
        },
        {
            name: 'logout',
            text: 'Log out',
            onClick: () => {
                localStorage.removeItem('token')
                dispatch({ type: "LOGOUT", payload: null })
                router.push('/')
            }
        }
    ]

    const toggleSidebar = () => {
        dispatch({ type: "TOGGLE_SIDEBAR", payload: null })
    }


    return (
        <div className="bg-white py-4 w-full shadow fixed top-0 z-50">
            <Container size="wide">
                <div className="flex items-center justify-between">
                    <div className="lg:hidden block">
                        <Button variant="light" onClick={toggleSidebar}>
                            <FontAwesomeIcon icon={faBars} />
                        </Button>
                    </div>
                    <div className="w-28">
                        <Slink href="/">
                            <Image src="/logo.png" layout="responsive" width={100} height={38} alt="logo-storin" />
                        </Slink>
                    </div>
                    <div className="flex gap-x-2">
                        <NavDropdown menuList={authMenu}>
                            <FontAwesomeIcon icon={faUser} className="mr-2" />
                            { globalState.auth.isUserAuthenticated ? globalState.auth.loggedUser.username : '' }
                            <FontAwesomeIcon icon={faChevronDown} className="text-sm ml-2" />
                        </NavDropdown>
                        {/* <Button variant="primary">
                            Login
                        </Button> */}
                        {/* <Button variant="light">
                            Register
                        </Button> */}
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default NavbarDashboard