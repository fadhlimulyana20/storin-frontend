import { faBox, faBoxes, faCog, faFileInvoice, faHome, faUserGroup } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useState } from "react"
import VerticalNavGroup from '../../molecules/navGroup/vertical'

interface Props {
    hidden ?: boolean
}

const DashboardSidebar = ({ hidden=false }: Props) => {
    const mobileShowClass = 'fixed z-20 h-full top-0 left-0 pt-16 flex lg:flex flex-shrink-0 flex-col w-64 transition-transform duration-75'
    const normalShowClass = 'fixed hidden z-20 h-full top-0 left-0 pt-16 flex lg:flex flex-shrink-0 flex-col w-64 transition-transform duration-75'

    const [menu] = useState([
        {
            display: (
                <div>
                    <FontAwesomeIcon icon={faHome} fixedWidth={true} />
                    <span className="ml-2">Beranda</span>
                </div>
            ), 
            name: 'Home',
            href: '/dashboard',
            index: true
        },
        {
            display: (
                <div>
                    <FontAwesomeIcon icon={faBoxes} fixedWidth={true} />
                    <span className="ml-2">Produk</span>
                </div>
            ), 
            name: 'Product',
            href: '/dashboard/products',
        },
        {
            display: (
                <div>
                    <FontAwesomeIcon icon={faUserGroup} fixedWidth={true} />
                    <span className="ml-2">Pelanggan</span>
                </div>
            ), 
            name: 'Customers',
            href: '/dashboard/customers',
        },
        {
            display: (
                <div>
                    <FontAwesomeIcon icon={faFileInvoice} fixedWidth={true} />
                    <span className="ml-2">Invoice Penjualan</span>
                </div>
            ), 
            name: 'Invoices',
            href: '/dashboard/invoices',
        },
        {
            display: (
                <div>
                    <FontAwesomeIcon icon={faCog} fixedWidth={true} />
                    <span className="ml-2">Pengaturan</span>
                </div>
            ), 
            name: 'Setting',
            href: '/dashboard/settings',
        }
    ])

    return (
        <div className={ hidden ? normalShowClass : mobileShowClass }>
            <div className="relative flex-1 flex flex-col min-h-0 border-r border-gray-200 bg-white pt-0">
                <div className="flex-1 px-3 bg-white divide-y space-y-1 hover:overflow-y-scroll">
                    <ul className="space-y-2 pb-2 pt-5">
                        <VerticalNavGroup menu={menu} />
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default DashboardSidebar