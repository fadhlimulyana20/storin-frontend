import { faInstagram, faTwitter } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Image from "next/image"
import Container from "../../atoms/container"

const Footer = () => {
    return (
        <div className="bg-blue-900 py-4">
            <Container>
                <div className="flex justify-between items-center">
                    <div className="w-24">
                        <Image src="/images/storin-white.png" layout="responsive" height={83} width={226} alt="storin-logo-white" />
                    </div>
                    <div className="flex gap-x-1 text-white">
                        <FontAwesomeIcon icon={faTwitter} />
                        <FontAwesomeIcon icon={faInstagram} />
                    </div>
                </div>
            </Container>
        </div>
    )
}

export default Footer