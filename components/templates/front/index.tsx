import axios, { AxiosResponse } from "axios"
import { ReactNode, useContext, useEffect } from "react"
import toast from "react-hot-toast"
import { Context } from "../../../contexts/store"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"
import Footer from "../../organisms/footer"
import Navbar from "../../organisms/navbar"

interface Props {
    children: ReactNode | any
}

const FrontLayout = ({ children }: Props) => {
    const { globalState, dispatch } = useContext(Context)

    useEffect(() => {
        const token = localStorage.getItem('token')

        const fetchAuthenticatedUser = async () => {
            if (token) {
                try {
                    const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/auth/me', {
                        headers: {
                            Authorization: `Bearer ${token}`
                        }
                    })
    
                    if (res.status === 200) {
                        dispatch({ type: "SET_USER", payload: res.data.data.user })
                    }
                } catch (e: any) {
                    if (e.response) {
                        dispatch({ type: "LOGOUT", payload: null })
                    } else {
                        alert(e)
                    }
                }
            }
        }

        fetchAuthenticatedUser()
    }, [dispatch])
    return (
        <div>
            <Navbar />
            <div className="min-h-screen">
                {children}
            </div>
            <Footer />
        </div>
    )
}

export default FrontLayout