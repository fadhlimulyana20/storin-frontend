import { faBarsProgress, faCircle, faCircleHalfStroke, faCircleNotch, faCog, faCogs, faSpinner, faTruckLoading } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import Image from "next/image"
import { useRouter } from "next/router"
import { ReactNode, useContext, useEffect, useState } from "react"
import toast from "react-hot-toast"
import { Context } from "../../../contexts/store"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"
import DefaultLoadingScreen from "../../molecules/loadingScreen"
import NavbarDashboard from "../../organisms/navbar/dasboard"
import DashboardSidebar from "../../organisms/sidebar/dashboard"

interface Props {
    children: ReactNode | any
}

const Dashboard = ({ children }: Props) => {
    const { globalState, dispatch } = useContext(Context);
    const router = useRouter()
    const [sidebarHidden, setSidebarHidden] = useState(true)
    const [isLoading, setIsLoading] = useState(true)
    // const [loadingMyStore, setLoadingMyStore] = useState(false)
    const [myStore, setMyStore] = useState<any>(null)
    const [user, setUser] = useState<any>()

    useEffect(() => {
        if (globalState.dashboard.isSidebarHidden) {
            setSidebarHidden(true)
        } else {
            setSidebarHidden(false)
        }
    }, [globalState.dashboard.isSidebarHidden])

    // useEffect(() => {
    //     const token = localStorage.getItem('token')

    //     const fetchAuthenticatedUser = async () => {
    //         if (token) {
    //             try {
    //                 const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/auth/me', {
    //                     headers: {
    //                         Authorization: `Bearer ${token}`
    //                     }
    //                 })
    
    //                 if (res.status === 200) {
    //                     dispatch({ type: "SET_USER", payload: res.data.data.user })
    //                 }
    //             } catch (e: any) {
    //                 if (e.response) {
    //                     dispatch({ type: "LOGOUT", payload: null })
    //                 } else {
    //                     alert(e)
    //                 }
    //                 router.push("/login")
    //             }
    //         }
    //     }

    //     fetchAuthenticatedUser()
    // }, [dispatch])

    useEffect(() => {
        const token = localStorage.getItem('token')

        const fetchAuthenticatedUser = async () => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/auth/me', {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    dispatch({ type: "SET_USER", payload: res.data.data.user })
                    setUser(res.data.data.user)
                }
            } catch (e: any) {
                if (e.response) {
                    dispatch({ type: "LOGOUT", payload: null })
                    router.push("/login" + "?next=" + router.pathname)
                    toast.error(e.response.data.message)
                } else {
                    alert(e)
                }
            }
        }

        const fetchMyStore = async () => {
            // const userId = globalState.auth.loggedUser.id
            // setIsLoading(true)
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/user/" + user.id)
    
                if (res.status === 200) {
                    // setMyStore(res.data.data)
                    dispatch({ type: "SET_MY_STORE", payload: res.data.data })
                }
            } catch (e: any) {
                console.log(e)
            } finally {
                setIsLoading(false)
            }
        }

        // if (globalState.auth.loggedUser) {
        //     fetchMyStore()
        // } else {
        //     fetchAuthenticatedUser()
        // }

        fetchAuthenticatedUser().then(r => {
            if (user) {
                fetchMyStore()
            }
        })
    }, [dispatch, router, user?.id])
    
    // useEffect(() => {
    //     const fetchMyStore = async () => {
    //         const userId = globalState.auth.loggedUser.id
    //         setLoadingMyStore(true)
    
    //         try {
    //             const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/user/" + userId)
    
    //             if (res.status === 200) {
    //                 setMyStore(res.data.data)
    //             }
    //         } catch (e: any) {
    //             console.log(e)
    //         } finally {
    //             setLoadingMyStore(false)
    //         }
    //     }

    //     if (globalState.auth.loggedUser) {
    //         fetchMyStore()
    //     }
    // }, [globalState.auth.loggedUser, globalState.auth.loggedUser?.id])

    useEffect(() => {
        if (router.pathname !== "/dashboard/customers/edit/[id]") {
            dispatch({type: "UNSET_CUSTOMER", payload: null})
        }
    }, [dispatch, router.pathname])
    

    return (
        <div>
            {isLoading ? (
                <div>
                    <DefaultLoadingScreen />
                </div>
            ) : (
                <div>
                    <NavbarDashboard />
                    <div className="flex overflow-hidden bg-white pt-16">
                        <DashboardSidebar hidden={sidebarHidden} />
                        <div className="min-h-screen w-full bg-gray-50 relative overflow-y-auto lg:ml-64">
                            {children}
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default Dashboard