import Image from "next/image"
import { ReactNode } from "react"
import Container from "../../atoms/container"
import Slink from "../../atoms/links/link"

interface Props {
    children: ReactNode | any
}

export default function ShareTemplate({ children }: Props) {
    return (
        <div>
            {children}
            <Container className="py-5">
                <div className="w-24 mx-auto">
                    <h5 className="text-center text-sm font-bold mb-1">Invoice By</h5>
                    <Slink href="/">
                        <Image src="/logo.png" layout="responsive" width={100} height={38} alt="logo-storin" />
                    </Slink>
                </div>
            </Container>
        </div>
    )
}