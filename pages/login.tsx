import Image from 'next/image'
import { faRightToBracket } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { Formik } from "formik"
import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react"
import Alert from "../components/atoms/alert"
import Button from "../components/atoms/button"
import Container from "../components/atoms/container"
import FormError from "../components/atoms/input/formError"
import FormInput from "../components/atoms/input/formInput"
import FormLabel from "../components/atoms/input/formLabel"
import Slink from "../components/atoms/links/link"
import Header from "../components/molecules/header"
import FrontLayout from "../components/templates/front"
import { Context } from "../contexts/store"
import { backendUrl } from "../helpers/constants"
import { JsonResponse } from "../helpers/type"
import useScript from "../hooks/useScripts"

const LoginPage = () => {
    const { globalState, dispatch } = useContext(Context);
    const router = useRouter()
    const [showErrorAlert, setShowErrorAlert] = useState(false)
    const [errorLogin, setErrorLogin] = useState('')
    const [userId, setUserId] = useState("")

    const normalLogin = async (email: string, password: string) => {
        try {
            const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + '/auth/login', { email, password })
            if (res.status === 200) {
                localStorage.setItem('token', res.data.data.token)
                dispatch({ type: "SET_USER", payload: res.data.data.user })

                const resInternalLogin: AxiosResponse<any> = await axios.post('/api/login', { token: res.data.data.token })
                if (resInternalLogin.status === 200) {
                    // router.push('/registration/create-store')
                    setUserId(res.data.data.user.id)
                }
            }
        } catch (e: any) {
            if (e.response) {
                setErrorLogin(e.response.data.message)
                setShowErrorAlert(true)
            } else {
                alert(e)
            }
        }
    }

    useEffect(() => {
        const fetchMyStore = async (userId: string) => {
            // const userId = globalState.auth.loggedUser.id
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/user/" + userId)

                if (res.status === 200) {
                    console.log(res)
                    router.push("/dashboard")
                }
            } catch (e: any) {
                router.push("/registration/create-store")
            }
        }
        if (globalState.auth.isUserAuthenticated) {
            fetchMyStore(userId)
        }
    }, [globalState.auth.isUserAuthenticated, router, userId])

    useScript("https://accounts.google.com/gsi/client")
    // useScript("/login.min.js")

    useEffect(() => {
        const script = document.createElement('script');

        script.text = `function parseJwt(e){var o=e.split(".")[1].replace(/-/g,"+").replace(/_/g,"/"),n=decodeURIComponent(atob(o).split("").map(function(e){return"%"+("00"+e.charCodeAt(0).toString(16)).slice(-2)}).join(""));return JSON.parse(n)}async function onSignIn(e){const o=parseJwt(e.credential);console.log("ID: "+o.sub),console.log("Full Name: "+o.name),console.log("Given Name: "+o.given_name),console.log("Family Name: "+o.family_name),console.log("Image URL: "+o.picture),console.log("Email: "+o.email);try{const e={method:"POST",headers:{Accept:"application/json","Content-Type":"application/json"},body:JSON.stringify({name:o.name,email:o.email,username:o.given_name})},n=await fetch("/api/auth/google",e),a=await n.json();localStorage.setItem("token",a.data.token),window.location.href="/auth/google"}catch(e){console.log(e)}}`;
        script.async = true;

        document.body.appendChild(script);

        return () => {
            document.body.removeChild(script);
        }
    }, []);

    return (
        <FrontLayout>
            <Header
                title="Masuk Akun Storin"
                description="Kelola Produk dan Penjualanmu dari Satu Tempat dengan Mudah. Beralih ke pencatatan penjualan digital, buat invoice, kelola produk, dan lihat laporan bulanan lewat Storin."
            >
                <meta name="google-signin-client_id" content="709110421014-de1jtcnbgnos0dkgl4gf5fhdhk27mvc0.apps.googleusercontent.com"></meta>
            </Header>
            <div className="grid lg:grid-cols-2">
                <div className="bg-blue-500 min-h-screen hidden lg:inline">
                    <Container className="pt-24 text-white flex items-center min-h-screen">
                        <div className="w-full text-center">
                            <div className="mb-5">
                                <Image src="/images/invoice.png" layout="responsive" height={406} width={696} alt="storin" />
                            </div>
                            <h5 className="text-lg">Buat faktur dan kelola penjualan bisnismu melalui storin.</h5>
                        </div>
                    </Container>
                </div>
                <div>
                    <Container className="pt-24">
                        <div className="xl:px-24">
                            <div className="text-center mb-5">
                                <FontAwesomeIcon icon={faRightToBracket} className="text-5xl text-blue-500 mb-5" />
                                <h1 className="text-3xl font-bold">Halo Lagi!</h1>
                                <h5 className="text-gray-500 text-sm">Selamat datang di Storin, aplikasi pendamping bisnis untuk memudahkan administrasi bisnis kamu.</h5>
                            </div>
                            <Formik
                                initialValues={{ email: '', password: '' }}
                                validate={values => {
                                    const errors: any = {}
                                    if (!values.email) {
                                        errors.email = 'Email harus diisi'
                                    } else if (
                                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                                    ) {
                                        errors.email = 'Email tidak valid';
                                    }
                                    return errors;
                                }}
                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(true)
                                    normalLogin(values.email, values.password).then(r => {
                                        setSubmitting(false)
                                    })
                                }}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                    isSubmitting,
                                }) => (
                                    <form onSubmit={handleSubmit}>
                                        <Alert variant="error" className="mb-5" show={showErrorAlert}>
                                            {errorLogin}
                                        </Alert>
                                        <div className="mb-5">
                                            <FormLabel htmlFor="email">Email</FormLabel>
                                            <FormInput
                                                type={"email"}
                                                placeholder="Masukan Email"
                                                id="email"
                                                name="email"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                            />
                                            <FormError>
                                                {errors.email && touched.email && errors.email}
                                            </FormError>
                                        </div>
                                        <div className="mb-5">
                                            <FormLabel htmlFor="password">Password</FormLabel>
                                            <FormInput
                                                type={"password"}
                                                placeholder="Masukan Password"
                                                id="password"
                                                name="password"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.password}
                                            />
                                            <FormError>
                                                {errors.password && touched.password && errors.password}
                                            </FormError>
                                        </div>
                                        <div className="flex items-center mb-5 justify-end">
                                            <input type="checkbox" name="" id="" className="h-5 w-5" />
                                            <span className="ml-2">Ingat saya</span>
                                        </div>

                                        <Button variant="primary" className="w-full py-3 mb-4" type="submit" disabled={isSubmitting}>
                                            {isSubmitting ? 'Memproses...' : 'Masuk'}
                                        </Button>
                                    </form>
                                )}
                            </Formik>
                            <p className="text-center mb-2">atau</p>
                            <div id="g_id_onload"
                                data-client_id="709110421014-de1jtcnbgnos0dkgl4gf5fhdhk27mvc0.apps.googleusercontent.com"
                                data-callback="onSignIn"
                                >
                            </div>
                            <div className="flex justify-center">
                                <div className="g_id_signin" data-type="standard" data-size="large" data-width="320"></div>
                            </div>
                            <div className="mt-10">
                                <h5 className="text-center">
                                    <span className="mr-2">Belum memiliki akun?</span>
                                    <Slink href="/register">buat akun sekarang</Slink>
                                </h5>
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
        </FrontLayout>
    )
}

export default LoginPage