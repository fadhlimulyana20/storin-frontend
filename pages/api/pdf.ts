// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import pdf from 'html-pdf'
import { PdfTemplate } from '../../helpers/pdf/template';
import fs from 'fs'

type Data = {
  name: string
}


export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const data = {
    name: [
      {
        name: "test"
      },
      {
        name: "yes"
      }
    ],
    price1: 1,
    price2: 2,
    receiptId: 3
  }

  pdf.create(PdfTemplate(data)).toStream(function(err, stream){
    // return res.status(200).send(stream.pipe(fs.createWriteStream('./foo.pdf')));
    if (err) {   
      // handle error and return a error response code
      console.log(err)
      return res.status(500)
    } else {
      // send a status code of 200 OK
      res.statusCode = 200             

      // once we are done reading end the response
      stream.on('end', () => {
        // done reading
        return res.end()
      })

      // pipe the contents of the PDF directly to the response
      stream.pipe(res)
    }

  });
}
