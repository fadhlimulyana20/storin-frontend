// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import cookie from 'cookie'
import axios, { AxiosResponse } from 'axios'
import { JsonResponse } from '../../../helpers/type'
import { backendUrl } from '../../../helpers/constants'


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<any>
) {
    if (req.method === "POST") {
        try {
            const response: AxiosResponse<JsonResponse> = await axios.post(backendUrl + "/auth/social-register", {
                name: req.body.name,
                email: req.body.email,
                username: req.body.username,
                registration_type: "google"
            })

            res.setHeader("Set-Cookie", cookie.serialize("token", response.data.data.token, {
                httpOnly: true,
                secure: process.env.NODE_ENV !== 'development',
                maxAge: 60 * 60 * 24 * 7,
                sameSite: "strict",
                path: "/"
            }))
            res.status(200)
            res.json({ 
                success: true,  
                data: {
                    token: response.data.data.token,
                }
            })
        } catch(e: any){
            console.log(e)
            res.status(500)
            res.json({
                success: false
            })
        }

    }
}
