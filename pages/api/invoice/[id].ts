// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import pdf from 'html-pdf'
import { PdfTemplate } from '../../../helpers/pdf/template';
import fs from 'fs'
import axios from 'axios';
import { backendUrl } from '../../../helpers/constants';
import { dateString } from '../../../helpers/date';
import path from 'path';

type Data = {
    name: string
}


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<any>
) {
    const { id } = req.query
    const token = req.cookies.token

    try {
        const response = await axios.get(backendUrl + '/v1/transaction/' + id, {
            // headers: {
            //     Authorization: `Bearer ${token}`
            // }
        })

        const responseCustomer = await axios.get(backendUrl + '/v1/customer/' + response.data.data.customer_id, {
            // headers: {
            //     Authorization: `Bearer ${token}`
            // }
        })

        const responseStore = await axios.get(backendUrl + '/v1/store/user/' + response.data.data.user_id, {
            // headers: {
            //     Authorization: `Bearer ${token}`
            // }
        })

        const responseInvoiceConfig = await axios.get(backendUrl + '/v1/config/pub/invoice/store/' + responseStore.data.data.id, {
            // headers: {
            //     Authorization: `Bearer ${token}`
            // }
        })

        // console.log(responseInvoiceConfig)

        const data = {
            ...response.data.data,
            customer: { ...responseCustomer.data.data },
            store: { ...responseStore.data.data },
            config: { ...responseInvoiceConfig.data.data },
            issued_date: dateString(new Date(response.data.data.issued_date)),
            due_date: dateString(new Date(response.data.data.due_date)),
        }

        res.setHeader("Content-Type", "text/html")
        res.send(PdfTemplate(data))

        
        // if (process.env.NODE_ENV === "production") {
        //     process.env.FONTCONFIG_PATH = path.join(process.cwd(), "fonts");
        //     process.env.LD_LIBRARY_PATH = path.join(process.cwd(), "bins");
        // }

        // pdf.create(
        //     PdfTemplate(data), {
        //     // phantomPath: path.resolve(
        //     //     process.cwd(),
        //     //     'node_modules/phantomjs-prebuilt/lib/phantom/bin/phantomjs'
        //     // ),
        // }).toBuffer(function (err, stream) {
        //     // return res.status(200).send(stream.pipe(fs.createWriteStream('./foo.pdf')));
        //     if (err) {
        //         // handle error and return a error response code
        //         console.log(err)
        //         return res.status(500)
        //     } else {
        //         // // send a status code of 200 OK
        //         // res.statusCode = 200

        //         // // once we are done reading end the response
        //         // stream.on('end', () => {
        //         //     // done reading
        //         //     return res.end()
        //         // })

        //         // // pipe the contents of the PDF directly to the response
        //         // stream.pipe(res)

        //         res.setHeader('Content-Type', 'application/pdf');
        //         res.statusCode = 200;
        //         res.end(stream)
        //     }

        // });
    } catch (e: any) {
        res.status(500)
    }
}
