import axios, { AxiosResponse } from "axios";
import { useRouter } from "next/router";
import { useContext, useEffect, useMemo, useState } from "react";
import toast from "react-hot-toast";
import ShareTemplate from "../../../components/templates/share";
import { Context } from "../../../contexts/store";
import { CustomerInterface } from "../../../contexts/types/customerReducerType";
import { backendUrl } from "../../../helpers/constants";
import { convertISOtoDateForm, dateString } from "../../../helpers/date";
import { JsonResponse } from "../../../helpers/type";
import { v4 as uuidv4 } from 'uuid'
import FormInput from "../../../components/atoms/input/formInput";
import NumberFormat from "react-number-format";
import Button from "../../../components/atoms/button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faFilePdf, faPhone, faTrash } from "@fortawesome/free-solid-svg-icons";
import { useTable } from "react-table";
import Container from "../../../components/atoms/container";
import { Table, TableBody, TableBodyRow, TableDataCell, TableHead, TableHeaderCell, TableHeadRow } from "../../../components/atoms/table";
import Select from 'react-select'
import Image from "next/image";

export default function PublicInvoiceDetail() {
    const router = useRouter()

    const { globalState, dispatch } = useContext(Context)

    const [isFetchingTransaction, setIsFetchingTransaction] = useState(false)
    const [dueDate, setDueDate] = useState<String | any>("")
    const [issuedDate, setIssuedDate] = useState<String | any>("")
    const [customer, setCustomer] = useState<CustomerInterface>({
        id: '',
        email: '',
        name: '',
        phone_number: ''
    })
    const [noInvoice, setNoInvoice] = useState("")
    const [store, setStore] = useState<any>()
    const [isPaid, setIsPaid] = useState(false)

    const [productList, setProductList] = useState<Array<any>>([])

    useEffect(() => {
        const { id } = router.query

        const fetchStore = async (userId: string) => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/v1/store/user/' + userId)
                if (res.status === 200) {
                    setStore(res.data.data)
                    console.log(store)
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        const fetchCustomerDetail = async (customer_id: string) => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/v1/customer/' + customer_id)

                if (res.status === 200) {
                    setCustomer(res.data.data)
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        const fetchData = async () => {
            setIsFetchingTransaction(true)
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/v1/transaction/' + id)

                if (res.status === 200) {
                    setDueDate(convertISOtoDateForm(new Date(res.data.data.due_date)))
                    setIssuedDate(convertISOtoDateForm(new Date(res.data.data.issued_date)))
                    setProductList(res.data.data.items)
                    setNoInvoice(res.data.data.no_invoice_string)
                    setIsPaid(res.data.data.is_paid)
                    fetchStore(res.data.data.user_id)
                    fetchCustomerDetail(res.data.data.customer_id)
                    // console.log(res.data.data)
                    // if (res.data.data.is_paid) {
                    //     setPaymentStatus({ value: true, label: 'Lunas' })
                    // } else {
                    //     setPaymentStatus({ value: false, label: 'Belum Bayar' })
                    // }
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            } finally {
                setIsFetchingTransaction(false)
            }
        }

        if (id) {
            fetchData()
        }
    }, [router.query])

    useEffect(() => {
        if (productList.length > 0 && !isFetchingTransaction) {
            let pro = [...productList]
            pro.forEach(pl => {
                pl.pseudoId = uuidv4().toString()
                console.log(pl)
                console.log(pl.pseudoId)
            })
            setProductList(pro)
        }

    }, [isFetchingTransaction])

    const columns = useMemo(
        () => [
            {
                Header: 'SKU',
                accessor: 'sku',
            },
            {
                Header: 'Nama',
                accessor: 'name',
            },
            {
                Header: 'Kuantitas',
                accessor: 'qty',
            },
            {
                Header: 'Diskon',
                accessor: 'discountInput',
            },
            {
                Header: 'Pajak',
                accessor: 'taxInput',
            },
            {
                Header: 'Harga',
                accessor: 'priceDisplay',
            },
            {
                Header: 'Total',
                accessor: 'total',
            },
        ],
        []
    )

    const data = useMemo(
        (): any => {
            if (productList) {
                let pro = [...productList]
                pro.forEach((p, idx) => {
                    p.qtyInput = (
                        <FormInput
                            className="w-20"
                            type={"number"}
                            value={p.qty}
                            onChange={(e) => {
                                let productListCopy = [...productList]
                                productListCopy[idx].qty = e.target.value
                                setProductList(productListCopy)
                            }}
                            full={false}
                        />
                    )
                    p.discountInput = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," suffix="%" displayType="text" value={p.discount} />
                    )
                    p.taxInput = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," suffix="%" displayType="text" value={p.tax} />
                    )
                    p.priceDisplay = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={p.price} />
                    )
                    p.total = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={(p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((100 + parseFloat(p.tax)) / 100)} />
                    )
                    p.action = (
                        <div className="flex gap-x-2">
                            <Button
                                variant="outline-danger"
                                className="text-sm px-1"
                                title="hapus"
                                onClick={() => {
                                    setProductList([...productList.filter(pl => pl.pseudoId !== p.pseudoId)])
                                }}
                            >
                                <FontAwesomeIcon icon={faTrash} />
                            </Button>
                        </div>
                    )
                })

                return pro
            } else {
                return []
            }
        },
        [productList]
    )

    const tableInstance = useTable({ columns, data })

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = tableInstance

    const PrintInvoice = () => {
        const { id } = router.query
        window.open('/api/invoice/' + id, "_blank")
    }

    return (
        <ShareTemplate>
            <Container className="pt-16">
                <div className="bg-white border rounded-lg shadow px-6 py-4 mb-5">
                    <div className="flex mb-2 justify-end">
                        {isPaid ? (
                            <div className="bg-green-500 text-white px-2 rounded-full text-sm">
                                Lunas
                            </div>
                        ) : (
                            <div className="bg-red-500 text-white px-2 rounded-full text-sm">
                                Belum bayar
                            </div>
                        )}
                    </div>
                    <div className="flex justify-between mb-5 border-b-2 pb-4">
                        <Image src={store ? store.path_logo : "/logo-square.jpg"} layout="fixed" width={80} height={80} alt={store ? store.name : "logo"} />
                        <div className="md:w-1/6 text-right">
                            <div className="">
                                <h5 className="text-lg">Invoice #: {noInvoice}</h5>
                            </div>
                            <div className="">
                                <h5><span className="font-bold">Diterbitkan: </span> {dateString(new Date(issuedDate))}</h5>
                            </div>
                            <div className="">
                                <h5><span className="font-bold">Jatuh Tempo: </span>{dateString(new Date(dueDate))}</h5>
                            </div>
                        </div>
                    </div>
                    <div className="flex justify-between mb-5">
                        <div>
                            <h5 className="font-bold text-lg">Dari</h5>
                            <h5>{store ? store.name : ""}</h5>
                        </div>
                        <div className="text-right">
                            <h5 className="font-bold text-lg">Kepada</h5>
                            <div className="mb-4">
                                <h5>{customer.name || 'Nama Pelanggan'}</h5>
                                <h5 className="text-sm">
                                    <FontAwesomeIcon icon={faEnvelope} fixedWidth className="mr-1 text-blue-600" />
                                    {customer.email}
                                </h5>
                                <h5 className="text-sm">
                                    <FontAwesomeIcon icon={faPhone} fixedWidth className="mr-1 text-blue-600" />
                                    {customer.phone_number}
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="mb-4">
                            <Table {...getTableProps()}>
                                <TableHead>
                                    {headerGroups.map((headerGroup, idx) => (
                                        <TableHeadRow {...headerGroup.getHeaderGroupProps()} key={idx}>
                                            {headerGroup.headers.map((column, idx) => (
                                                <TableHeaderCell {...column.getHeaderProps()} key={idx}>
                                                    {column.render('Header')}
                                                </TableHeaderCell>
                                            ))}
                                        </TableHeadRow>
                                    ))}
                                </TableHead>
                                <TableBody {...getTableBodyProps()}>
                                    {// Loop over the table rows
                                        rows.map((row, idx) => {
                                            // Prepare the row for display
                                            prepareRow(row)
                                            return (
                                                // Apply the row props
                                                <TableBodyRow className="hover:cursor-pointer" {...row.getRowProps()} key={idx}>
                                                    {// Loop over the rows cells
                                                        row.cells.map((cell, idx) => {
                                                            // Apply the cell props
                                                            return (
                                                                <TableDataCell   {...cell.getCellProps()} key={idx}>
                                                                    {// Render the cell contents
                                                                        cell.render('Cell')}
                                                                </TableDataCell>
                                                            )
                                                        })}
                                                </TableBodyRow>
                                            )
                                        })}
                                </TableBody>
                            </Table>
                        </div>

                        <div className="flex justify-end">
                            <div className="rounded-lg overflow-auto border">
                                <table className="">
                                    <tr className="border-b">
                                        <th className="border-r px-2 text-left">Total Normal</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                    <tr className="border-b">
                                        <th className="border-r px-2 text-left">Diskon</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price) * ((parseFloat(p.discount)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                    <tr className="border-b">
                                        <th className="border-r px-2 text-left">Pajak</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((parseFloat(p.tax)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                    <tr className="border-b bg-blue-50">
                                        <th className="border-r px-2 text-left text-blue-600">Total</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((100 + parseFloat(p.tax)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </Container>
        </ShareTemplate>
    )
}