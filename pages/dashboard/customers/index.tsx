import { useRouter } from "next/router"
import { useCallback, useContext, useEffect, useMemo, useState } from "react"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import FormInput from "../../../components/atoms/input/formInput"
import { Table, TableBody, TableBodyRow, TableDataCell, TableHead, TableHeaderCell, TableHeadRow } from "../../../components/atoms/table"
import Header from "../../../components/molecules/header"
import Modal from "../../../components/molecules/modal"
import Dashboard from "../../../components/templates/dashboard"
import { useTable } from 'react-table'
import axios, { AxiosResponse } from "axios"
import { JsonResponse } from "../../../helpers/type"
import { backendUrl } from "../../../helpers/constants"
import toast from "react-hot-toast"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons"
import { Context } from "../../../contexts/store"

const CustomerPage = () => {
    const { dispatch, globalState } = useContext(Context)
    const router = useRouter()
    const [customers, setCustomers] = useState<Array<any>>([])
    const [openModal, setOpenModal] = useState(false)
    const [customerId, setCustomerId] = useState("")

    const editCustomer = useCallback(({ id, name, phone_number, email }: any) => {
        dispatch({ type: "SET_CUSTOMER", payload: { id, name, phone_number, email } })
        router.push(router.pathname + `/edit/${id}`)
    }, [dispatch, router])

    useEffect(() => {
        const fetchData = async () => {
            const token = localStorage.getItem('token')

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/customer/my', {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    setCustomers(res.data.data)
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        fetchData()
    }, [])

    const deleteCustomer = async (id: string) => {
        const token = localStorage.getItem("token")

        try {
            const res: AxiosResponse<JsonResponse> = await axios.delete(backendUrl + "/customer/my/delete/" + id, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                setCustomers([...customers.filter(p => p.id !== id)])
                setCustomerId("")
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                alert(e)
            }
        } finally {
            setOpenModal(false)
        }
    }

    const data = useMemo(
        (): any => {
            if (customers) {
                let cust = [...customers]
                cust.forEach(c => {
                    c.action = (
                        <div className="flex gap-x-2">
                            <Button
                                variant="outline-primary"
                                className="text-sm px-1"
                                title="edit"
                                onClick={() => editCustomer(c)}
                            >
                                <FontAwesomeIcon icon={faEdit} />
                            </Button>
                            <Button
                                variant="outline-danger"
                                className="text-sm px-1"
                                title="hapus"
                                onClick={() => {
                                    setCustomerId(c.id)
                                    setOpenModal(true)
                                }}
                            >
                                <FontAwesomeIcon icon={faTrash} />
                            </Button>
                        </div>
                    )
                })

                return cust
            } else {
                return []
            }
        },
        [customers, editCustomer]
    )

    const columns = useMemo(
        () => [
            {
                Header: 'Nama',
                accessor: 'name', // accessor is the "key" in the data
            },
            {
                Header: 'No Telepon',
                accessor: 'phone_number',
            },
            {
                Header: 'Email',
                accessor: 'email',
            },
            {
                Header: '',
                accessor: 'action',
            },
        ],
        []
    )

    const tableInstance = useTable({ columns, data })

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = tableInstance

    return (
        <Dashboard>
            <Header
                title="Pelanggan"
                description="Daftar Pelanggan"
            />
            <Modal
                open={openModal}
                setOpen={setOpenModal}
                type="question"
                title="Hapus Pelanggan"
                onConfirm={() => deleteCustomer(customerId)}
            >
                Apakah anda yakin ingin menghapus pelanggan ini?
            </Modal>
            <Container className="pt-10" size="wide">
                <div className="flex justify-between items-center mb-5">
                    <h1 className="text-2xl font-bold">Pelanggan</h1>
                    <Button
                        link
                        className="py-1"
                        href={router.pathname + '/create'}
                    >
                        Tambah
                    </Button>
                </div>
                <Table {...getTableProps()}>
                    <TableHead>
                        {headerGroups.map((headerGroup, idx) => (
                            <TableHeadRow {...headerGroup.getHeaderGroupProps()} key={idx}>
                                {headerGroup.headers.map((column, idx) => (
                                    <TableHeaderCell {...column.getHeaderProps()} key={idx}>
                                        {column.render('Header')}
                                    </TableHeaderCell>
                                ))}
                            </TableHeadRow>
                        ))}
                    </TableHead>
                    <TableBody {...getTableBodyProps()}>
                        {// Loop over the table rows
                            rows.map((row, idx) => {
                                // Prepare the row for display
                                prepareRow(row)
                                return (
                                    // Apply the row props
                                    <TableBodyRow className="hover:cursor-pointer" {...row.getRowProps()} key={idx}>
                                        {// Loop over the rows cells
                                            row.cells.map((cell, idx) => {
                                                // Apply the cell props
                                                return (
                                                    <TableDataCell {...cell.getCellProps()} key={idx}>
                                                        {// Render the cell contents
                                                            cell.render('Cell')}
                                                    </TableDataCell>
                                                )
                                            })}
                                    </TableBodyRow>
                                )
                            })}
                    </TableBody>
                </Table>
            </Container>
        </Dashboard >
    )
}

export default CustomerPage