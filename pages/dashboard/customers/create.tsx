import { faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { Formik } from "formik"
import Router, { useRouter } from "next/router"
import toast from "react-hot-toast"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import FormError from "../../../components/atoms/input/formError"
import FormInput from "../../../components/atoms/input/formInput"
import FormLabel from "../../../components/atoms/input/formLabel"
import Header from "../../../components/molecules/header"
import Dashboard from "../../../components/templates/dashboard"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"

const CreateCustomer = () => {
    const router = useRouter()

    const saveCustomer = async ({ name, phone_number, email }: any) => {
        const token = localStorage.getItem('token')

        try {
            const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + "/customer/my/create", {
                name,
                phone_number,
                email
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                toast.success("Pelanggan berhasil ditambahkan")
                router.back()
            }
        } catch(e: any) {
            if (e.response) {
                if (e.response.data) {
                    toast.error(e.response.data.message)
                }
            } else {
                alert(e)
            }
        }
    }

    return (
        <Dashboard>
            <Header
                title="Tambah Pelanggan"
                description="Tambah Pelanggan"
            />
            <Container className="pt-10" size="wide">
                <h1 className="text-2xl font-bold mb-5">Tambah Pelanggan</h1>
                <div className="bg-white rounded p-5 shadow">
                    <Formik
                        enableReinitialize
                        initialValues={
                            {
                                name: '',
                                phone_number: '',
                                email: '',
                            }
                        }
                        validate={(values => {
                            const errors: any = {}

                            if (!values.name) {
                                errors.name = 'Nama harus diisi'
                            }

                            return errors;
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setSubmitting(true)
                            saveCustomer(values)
                                .then(r => {
                                    setSubmitting(false)
                                })
                        }}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                        }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="mb-5">
                                    <FormLabel htmlFor="name">Nama</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="Nama"
                                        id="name"
                                        name="name"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.name}
                                    />
                                    <FormError>
                                        {errors.name && touched.name && errors.name}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="phone_number">No Telepon</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="08124221xxx"
                                        id="phone_number"
                                        name="phone_number"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.phone_number}
                                    />
                                    <FormError>
                                        {errors.phone_number && touched.phone_number && errors.phone_number}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="email">Email</FormLabel>
                                    <FormInput
                                        type={"email"}
                                        placeholder="me@storin.id"
                                        id="email"
                                        name="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                    />
                                    <FormError>
                                        {errors.email && touched.email && errors.email}
                                    </FormError>
                                </div>

                                <div className="flex justify-end">
                                    <Button variant="primary" className="w-full md:w-32 py-2 " type="submit" disabled={isSubmitting}>
                                        {isSubmitting ? 'Menyimpan...' : (
                                            <div>
                                                <FontAwesomeIcon icon={faSave} className="mr-2" />
                                                Simpan
                                            </div>
                                        )}
                                    </Button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>
            </Container>
        </Dashboard>
    )
}

export default CreateCustomer