import { faEnvelope, faPhone, faSave, faSearch, faTrash } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { useContext, useEffect, useMemo, useState } from "react"
import toast from "react-hot-toast"
import NumberFormat from "react-number-format"
import { useTable } from "react-table"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import FormInput from "../../../components/atoms/input/formInput"
import { Table, TableBody, TableBodyRow, TableDataCell, TableHead, TableHeaderCell, TableHeadRow } from "../../../components/atoms/table"
import Header from "../../../components/molecules/header"
import Modal from "../../../components/molecules/modal"
import Dashboard from "../../../components/templates/dashboard"
import { Context } from "../../../contexts/store"
import { CustomerInterface } from "../../../contexts/types/customerReducerType"
import { backendUrl } from "../../../helpers/constants"
import { convertISOtoDateTimeForm } from "../../../helpers/date"
import { JsonResponse } from "../../../helpers/type"
import { v4 as uuidv4 } from 'uuid'
import Slink from "../../../components/atoms/links/link"
import { useRouter } from "next/router"

const CreateInvoice = () => {
    const { globalState, dispatch } = useContext(Context)
    const router = useRouter()

    const [savingInvoice, setSavingInvoice] = useState(false)
    const [open, setOpen] = useState(false)
    const [openModalProduct, setOpenModalPruct] = useState(false)
    const [openModalInvoiceConfigAlert, setOpenModalInvoiceConfigAlert] = useState(false)
    const [dueDate, setDueDate] = useState<String | any>("")
    const [issuedDate, setIssuedDate] = useState<String | any>("")
    const [customers, setCustomers] = useState<Array<CustomerInterface>>([])
    const [products, setProducts] = useState<Array<any>>([])
    const [customer, setCustomer] = useState<CustomerInterface>({
        id: '',
        email: '',
        name: '',
        phone_number: ''
    })

    const [productList, setProductList] = useState<Array<any>>([])

    const saveInvoice = async () => {
        const token = localStorage.getItem('token')
        setSavingInvoice(true)

        if (customer.id === '') {
            setSavingInvoice(false)
            return toast.error('Anda belum memilih pelanggan')
        }

        if (issuedDate === '') {
            setSavingInvoice(false)
            return toast.error('Anda belum mengisi tanggal diterbitkan')
        }

        if (dueDate === '') {
            setSavingInvoice(false)
            return toast.error('Anda belum mengisi tanggal jatuh tempo')
        }

        if (productList.length === 0) {
            setSavingInvoice(false)
            return toast.error('Anda belum menambahkan produk')
        }

        try {
            const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + '/transaction/my/create', {
                due_date: new Date(dueDate).toISOString(),
                issued_date: new Date(issuedDate).toISOString(),
                customer_id: customer.id,
                items: productList.map(p => {
                    return {
                        product_id: p.id,
                        qty: parseFloat(p.qty),
                        discount: parseFloat(p.discount),
                        tax: parseFloat(p.tax)
                    }
                })
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                router.push(`/dashboard/invoices/detail/${res.data.data.id}`)
                toast.success("Invoice berhasil dibuat")
            }
        } catch (e: any) {
            if (e.response) {
                if (e.response.data) {
                    toast.error(e.response.data.message)
                }
            } else {
                alert(e)
            }
        } finally {
            setSavingInvoice(false)
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            const token = localStorage.getItem('token')

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/customer/my', {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    if (res.data.data) {
                        setCustomers(res.data.data)
                    }
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        fetchData()
    }, [])

    useEffect(() => {
        const storeId = globalState.myStore.store?.id

        const fetchData = async () => {
            const token = localStorage.getItem('token')

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/product/user/' + storeId, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    if (res.data.data) {
                        setProducts(res.data.data)
                    }
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        if (storeId) {
            fetchData()
        }
    }, [globalState.myStore?.store?.id])

    const columns = useMemo(
        () => [
            {
                Header: 'SKU',
                accessor: 'sku',
            },
            {
                Header: 'Nama',
                accessor: 'name',
            },
            {
                Header: 'Kuantitas',
                accessor: 'qtyInput',
            },
            {
                Header: 'Diskon',
                accessor: 'discountInput',
            },
            {
                Header: 'Pajak',
                accessor: 'taxInput',
            },
            {
                Header: 'Harga',
                accessor: 'priceDisplay',
            },
            {
                Header: 'Total',
                accessor: 'total',
            },
            {
                Header: '',
                accessor: 'action',
            },
        ],
        []
    )

    const data = useMemo(
        (): any => {
            if (productList) {
                let pro = [...productList]
                pro.forEach((p, idx) => {
                    p.qtyInput = (
                        <FormInput
                            className="w-20"
                            type={"number"}
                            value={p.qty}
                            onChange={(e) => {
                                let productListCopy = [...productList]
                                productListCopy[idx].qty = e.target.value
                                setProductList(productListCopy)
                            }}
                            full={false}
                        />
                    )
                    p.discountInput = (
                        <div className="flex">
                            <FormInput
                                type={"number"}
                                className="w-20"
                                value={p.discount}
                                onChange={(e) => {
                                    let productListCopy = [...productList]
                                    productListCopy[idx].discount = e.target.value
                                    setProductList(productListCopy)
                                }}
                                full={false}
                            />
                            <div className="bg-blue-200 flex items-center justify-center px-3 rounded">
                                %
                            </div>
                        </div>
                    )
                    p.taxInput = (
                        <div className="flex">
                            <FormInput
                                type={"number"}
                                className="w-20"
                                value={p.tax}
                                onChange={(e) => {
                                    let productListCopy = [...productList]
                                    productListCopy[idx].tax = e.target.value
                                    setProductList(productListCopy)
                                }}
                                full={false}
                            />
                            <div className="bg-blue-200 flex items-center justify-center px-3 rounded">
                                %
                            </div>
                        </div>
                    )
                    p.priceDisplay = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={p.price} />
                    )
                    p.total = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={(p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((100 + parseFloat(p.tax)) / 100)} />
                    )
                    p.action = (
                        <div className="flex gap-x-2">
                            <Button
                                variant="outline-danger"
                                className="text-sm px-1"
                                title="hapus"
                                onClick={() => {
                                    setProductList([...productList.filter(pl => pl.pseudoId !== p.pseudoId)])
                                }}
                            >
                                <FontAwesomeIcon icon={faTrash} />
                            </Button>
                        </div>
                    )
                })

                return pro
            } else {
                return []
            }
        },
        [productList]
    )

    const tableInstance = useTable({ columns, data })

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = tableInstance

    useEffect(() => {
        const fetchInvoiceConfig = async () => {
            const token = localStorage.getItem('token')
            const { myStore } = globalState

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/v1/config/invoice/store/" + myStore.store.id, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

            } catch (e: any) {
                if (e.response.data) {
                    setOpenModalInvoiceConfigAlert(true)
                } else {
                    alert(e)
                }
            }
        }

        if (globalState.auth.isUserAuthenticated && globalState.myStore.store) {
            fetchInvoiceConfig()
        }

    }, [globalState])

    return (
        <Dashboard>
            <Header
                title="Buat Faktur"
                description="Buat Faktur"
            />

            <Modal
                id="invoice_config_alert"
                type="error"
                open={openModalInvoiceConfigAlert}
                setOpen={setOpenModalInvoiceConfigAlert}
                title="Peringatan"
                cancelText="Batal"
                confirmText="Atur Sekarang"
                onConfirm={() => {
                    router.push("/dashboard/settings/invoice")
                }}
            >
                Anda belum menambahkan pengaturan invoice. Atur pengaturan invoice anda terlebih dahulu agar invoice dapat dicetak secara sempurna.
            </Modal>

            <Modal
                plain
                open={open}
                setOpen={setOpen}
                title="Memilih Pelanggan"
                cancelText="Batal"
                confirmText="Simpan"
                noConfirmButton
            >
                <div className="mb-2 flex">
                    <FormInput
                        type={"text"}
                        placeholder="Cari Pelanggan"
                    />
                    <Button>
                        <FontAwesomeIcon icon={faSearch} />
                    </Button>
                </div>
                <div className="h-32 overflow-auto px-1">
                    {customers.length > 0 ? customers.map(c => {
                        return (
                            <div
                                className={`mb-1 border rounded px-2 py-1 w-full hover:bg-blue-50 text-gray-800 hover:cursor-pointer ${customer.id === c.id ? 'bg-blue-50' : ''}`}
                                key={c.id}
                                onClick={() => {
                                    setCustomer(c)
                                    setOpen(false)
                                }}
                            >
                                {c.name}
                            </div>
                        )
                    }) : (
                        <p>Anda belum memiliki pelanggan, <Slink href="/dashboard/customers/create">tambahkan sekarang</Slink></p>
                    )}
                </div>
            </Modal>

            <Modal
                plain
                open={openModalProduct}
                setOpen={setOpenModalPruct}
                title="Memilih Produk"
                cancelText="Batal"
                confirmText="Simpan"
                noConfirmButton
            >
                <div className="mb-2 flex">
                    <FormInput
                        type={"text"}
                        placeholder="Cari Produk"
                    />
                    <Button>
                        <FontAwesomeIcon icon={faSearch} />
                    </Button>
                </div>
                <div className="h-32 overflow-auto px-1">
                    {products.length > 0 ? products.map(c => {
                        return (
                            <div
                                className={`mb-1 border rounded px-2 py-1 w-full hover:bg-blue-50 text-gray-800 hover:cursor-pointer ${customer.id === c.id ? 'bg-blue-50' : ''}`}
                                key={c.id}
                                onClick={() => {
                                    const val = {
                                        id: c.id,
                                        name: c.name,
                                        sku: c.sku,
                                        price: c.price,
                                        qty: 0,
                                        discount: 0,
                                        tax: 0,
                                        pseudoId: uuidv4()
                                    }

                                    setProductList([...productList, val])
                                    setOpenModalPruct(false)
                                }}
                            >
                                <div>
                                    {c.name}
                                </div>
                                <div className="text-sm text-gray-500">
                                    {c.sku}
                                </div>
                            </div>
                        )
                    }) : (
                        <p>Anda belum memiliki produk, <Slink href="/dashboard/products/create">tambahkan sekarang</Slink></p>
                    )}
                </div>
            </Modal>

            <Container className="pt-10" size="wide">
                <h1 className="text-2xl font-bold mb-5">Buat Invoice</h1>
                <div className="bg-white rounded-lg shadow px-6 py-4 mb-5">
                    <div className="md:flex justify-between mb-5">
                        <div className="md:w-3/4 mb-5">
                            <h5 className="font-bold text-lg">Kepada</h5>
                            <div className="mb-4">
                                <h5>{customer.name || 'Nama Pelanggan'}</h5>
                                <h5 className="text-sm">
                                    <FontAwesomeIcon icon={faEnvelope} fixedWidth className="mr-1 text-blue-600" />
                                    {customer.email}
                                </h5>
                                <h5 className="text-sm">
                                    <FontAwesomeIcon icon={faPhone} fixedWidth className="mr-1 text-blue-600" />
                                    {customer.phone_number}
                                </h5>
                            </div>
                            <Button
                                variant="outline-primary"
                                onClick={() => setOpen(true)}

                            >
                                Pilih Pelanggan
                            </Button>
                        </div>
                        <div className="md:w-1/4">
                            <div className="mb-4">
                                <h5 className="font-bold text-lg">Diterbitkan</h5>
                                <FormInput
                                    value={issuedDate}
                                    onChange={(e) => setIssuedDate(e.target.value)}
                                    type={"date"}
                                />
                            </div>
                            <div className="mb-4">
                                <h5 className="font-bold text-lg">Jatuh Tempo</h5>
                                <FormInput
                                    value={dueDate}
                                    onChange={(e) => setDueDate(e.target.value)}
                                    type={"date"}
                                />
                            </div>
                        </div>
                    </div>

                    <div>
                        <div className="flex justify-end mb-2">
                            <Button
                                variant="outline-primary"
                                onClick={() => setOpenModalPruct(true)}
                            >
                                Pilih Produk
                            </Button>
                        </div>
                        <div className="mb-4">
                            <Table {...getTableProps()}>
                                <TableHead>
                                    {headerGroups.map((headerGroup, idx) => (
                                        <TableHeadRow {...headerGroup.getHeaderGroupProps()} key={idx}>
                                            {headerGroup.headers.map((column, idx) => (
                                                <TableHeaderCell {...column.getHeaderProps()} key={idx}>
                                                    {column.render('Header')}
                                                </TableHeaderCell>
                                            ))}
                                        </TableHeadRow>
                                    ))}
                                </TableHead>
                                <TableBody {...getTableBodyProps()}>
                                    {// Loop over the table rows
                                        rows.map((row, idx) => {
                                            // Prepare the row for display
                                            prepareRow(row)
                                            return (
                                                // Apply the row props
                                                <TableBodyRow className="hover:cursor-pointer" {...row.getRowProps()} key={idx}>
                                                    {// Loop over the rows cells
                                                        row.cells.map((cell, idx) => {
                                                            // Apply the cell props
                                                            return (
                                                                <TableDataCell {...cell.getCellProps()} key={idx}>
                                                                    {// Render the cell contents
                                                                        cell.render('Cell')}
                                                                </TableDataCell>
                                                            )
                                                        })}
                                                </TableBodyRow>
                                            )
                                        })}
                                </TableBody>
                            </Table>
                        </div>

                        <div className="flex justify-end">
                            <div className="rounded-lg overflow-auto border">
                                <table className="">
                                    <tr className="border-b">
                                        <th className="border-r px-2 text-left">Total Normal</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                    <tr className="border-b">
                                        <th className="border-r px-2 text-left">Diskon</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price) * ((parseFloat(p.discount)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                    <tr className="border-b">
                                        <th className="border-r px-2 text-left">Pajak</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((parseFloat(p.tax)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                    <tr className="border-b bg-blue-50">
                                        <th className="border-r px-2 text-left text-blue-600">Total</th>
                                        <td className="px-2">
                                            <NumberFormat
                                                thousandSeparator='.'
                                                decimalSeparator=","
                                                prefix='Rp. '
                                                displayType="text"
                                                value={productList.map(p => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((100 + parseFloat(p.tax)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                            />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="flex justify-end">
                    <Button
                        className="py-2 flex-1 md:flex-none"
                        disabled={savingInvoice}
                        onClick={() => saveInvoice()}
                    >
                        <FontAwesomeIcon icon={faSave} className="mr-2" />
                        <span>
                            {savingInvoice ? 'Menyimpan...' : 'Simpan'}
                        </span>
                    </Button>
                </div>
            </Container>
        </Dashboard>
    )
}

export default CreateInvoice