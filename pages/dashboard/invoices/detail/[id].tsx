import { faEnvelope, faFilePdf, faLink, faPhone, faSave, faSearch, faTrash } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { useRouter } from "next/router"
import { useContext, useEffect, useMemo, useState } from "react"
import toast from "react-hot-toast"
import NumberFormat from "react-number-format"
import { useTable } from "react-table"
import Button from "../../../../components/atoms/button"
import Container from "../../../../components/atoms/container"
import FormInput from "../../../../components/atoms/input/formInput"
import { Table, TableBody, TableBodyRow, TableDataCell, TableHead, TableHeaderCell, TableHeadRow } from "../../../../components/atoms/table"
import Header from "../../../../components/molecules/header"
import Modal from "../../../../components/molecules/modal"
import Dashboard from "../../../../components/templates/dashboard"
import { Context } from "../../../../contexts/store"
import { CustomerInterface } from "../../../../contexts/types/customerReducerType"
import { backendUrl, webURL } from "../../../../helpers/constants"
import { convertISOtoDateForm, convertISOtoDateTimeForm, dateString } from "../../../../helpers/date"
import { JsonResponse } from "../../../../helpers/type"
import { v4 as uuidv4 } from 'uuid'
import Select from 'react-select'

const CreateInvoice = () => {
    const { globalState, dispatch } = useContext(Context)
    const router = useRouter()

    const [isFetchingTransaction, setIsFetchingTransaction] = useState(false)
    const [savingInvoice, setSavingInvoice] = useState(false)
    const [open, setOpen] = useState(false)
    const [openModalProduct, setOpenModalPruct] = useState(false)
    const [dueDate, setDueDate] = useState<String | any>("")
    const [issuedDate, setIssuedDate] = useState<String | any>("")
    const [noInvoice, setNoInvoice] = useState<String>("")
    const [products, setProducts] = useState<Array<any>>([])
    const [customer, setCustomer] = useState<CustomerInterface>({
        id: '',
        email: '',
        name: '',
        phone_number: ''
    })

    

    const [productList, setProductList] = useState<Array<any>>([])

    const updatePaymentStatus = async (status: boolean) => {
        const token = localStorage.getItem('token')
        const { id } = router.query
        setSavingInvoice(true)

        try {
            const res: AxiosResponse<JsonResponse> = await axios.put(backendUrl + '/v1/transaction/my/payment/update-status/' + id, {
                is_paid: status
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                toast.success("Invoice berhasil dibuat")
            }
        } catch (e: any) {
            if (e.response) {
                if (e.response.data) {
                    toast.error(e.response.data.message)
                }
            } else {
                alert(e)
            }
        } finally {
            setSavingInvoice(false)
        }
    }

    useEffect(() => {
        const storeId = globalState.myStore.store?.id

        const fetchData = async () => {
            const token = localStorage.getItem('token')

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/product/user/' + storeId, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    if (res.data.data) {
                        setProducts(res.data.data)
                    }
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        if (storeId) {
            fetchData()
        }
    }, [globalState.myStore?.store?.id])

    useEffect(() => {
        const { id } = router.query

        const fetchCustomerDetail = async (customer_id: string) => {
            const token = localStorage.getItem('token')

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/customer/my/' + customer_id, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    if (res.data.data) {
                        setCustomer(res.data.data)
                    }
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        const fetchData = async () => {
            const token = localStorage.getItem('token')
            setIsFetchingTransaction(true)
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/transaction/my/' + id, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    setDueDate(convertISOtoDateForm(new Date(res.data.data.due_date)))
                    setIssuedDate(convertISOtoDateForm(new Date(res.data.data.issued_date)))
                    setNoInvoice(res.data.data.no_invoice_string)
                    setProductList(res.data.data.items)
                    fetchCustomerDetail(res.data.data.customer_id)
                    console.log(res.data.data)
                    if (res.data.data.is_paid) {
                        setPaymentStatus({value: true, label: 'Lunas'})
                    } else {
                        setPaymentStatus({value: false, label: 'Belum Bayar'})  
                    }
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            } finally {
                setIsFetchingTransaction(false)
            }
        }

        if (id) {
            fetchData()
        }
    }, [router.query])

    useEffect(() => {
        if (productList.length > 0 && !isFetchingTransaction) {
            let pro = [...productList]
            pro.forEach(pl => {
                pl.pseudoId = uuidv4().toString()
                console.log(pl)
                console.log(pl.pseudoId)
            })
            setProductList(pro)
        }

    }, [isFetchingTransaction])

    const columns = useMemo(
        () => [
            {
                Header: 'SKU',
                accessor: 'sku',
            },
            {
                Header: 'Nama',
                accessor: 'name',
            },
            {
                Header: 'Kuantitas',
                accessor: 'qty',
            },
            {
                Header: 'Diskon',
                accessor: 'discountInput',
            },
            {
                Header: 'Pajak',
                accessor: 'taxInput',
            },
            {
                Header: 'Harga',
                accessor: 'priceDisplay',
            },
            {
                Header: 'Total',
                accessor: 'total',
            },
        ],
        []
    )

    const data = useMemo(
        (): any => {
            if (productList) {
                let pro = [...productList]
                pro.forEach((p, idx) => {
                    p.qtyInput = (
                        <FormInput
                            className="w-20"
                            type={"number"}
                            value={p.qty}
                            onChange={(e) => {
                                let productListCopy = [...productList]
                                productListCopy[idx].qty = e.target.value
                                setProductList(productListCopy)
                            }}
                            full={false}
                        />
                    )
                    p.discountInput = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," suffix="%" displayType="text" value={p.discount} />
                    )
                    p.taxInput = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," suffix="%" displayType="text" value={p.tax} />
                    )
                    p.priceDisplay = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={p.price} />
                    )
                    p.total = (
                        <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={(p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((100 + parseFloat(p.tax)) / 100)} />
                    )
                    p.action = (
                        <div className="flex gap-x-2">
                            <Button
                                variant="outline-danger"
                                className="text-sm px-1"
                                title="hapus"
                                onClick={() => {
                                    setProductList([...productList.filter(pl => pl.pseudoId !== p.pseudoId)])
                                }}
                            >
                                <FontAwesomeIcon icon={faTrash} />
                            </Button>
                        </div>
                    )
                })

                return pro
            } else {
                return []
            }
        },
        [productList]
    )

    const tableInstance = useTable({ columns, data })

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = tableInstance

    const PrintInvoice = () => {
        const { id } = router.query
        window.open('/api/invoice/' + id, "_blank")
    }

    const paymentStatusOption = useMemo(() => [
        {value: true, label: 'Lunas'},
        {value: false, label: 'Belum Bayar'},
    ], [])
    const [paymentStatus, setPaymentStatus] = useState<any>()


    return (
        <Dashboard>
            <Header
                title="Buat Faktur"
                description="Buat Faktur"
            />

            <Modal
                plain
                open={openModalProduct}
                setOpen={setOpenModalPruct}
                title="Memilih Produk"
                cancelText="Batal"
                confirmText="Simpan"
                noConfirmButton
            >
                <div className="mb-2 flex">
                    <FormInput
                        type={"text"}
                        placeholder="Cari Produk"
                    />
                    <Button>
                        <FontAwesomeIcon icon={faSearch} />
                    </Button>
                </div>
                <div className="h-32 overflow-auto px-1">
                    {products.map(c => {
                        return (
                            <div
                                className={`mb-1 border rounded px-2 py-1 w-full hover:bg-blue-50 text-gray-800 hover:cursor-pointer ${customer.id === c.id ? 'bg-blue-50' : ''}`}
                                key={c.id}
                                onClick={() => {
                                    const val = {
                                        product_id: c.id,
                                        name: c.name,
                                        sku: c.sku,
                                        price: c.price,
                                        qty: 0,
                                        discount: 0,
                                        tax: 0,
                                        pseudoId: uuidv4()
                                    }

                                    setProductList([...productList, val])
                                    setOpenModalPruct(false)
                                }}
                            >
                                <div>
                                    {c.name}
                                </div>
                                <div className="text-sm text-gray-500">
                                    {c.sku}
                                </div>
                            </div>
                        )
                    })}
                </div>
            </Modal>

            <Container className="pt-10" size="wide">
                <h1 className="text-2xl font-bold mb-5">Detail Invoice</h1>
                <div className="md:grid md:grid-cols-6 gap-4">
                    <div className="col-span-4">
                        <div className="bg-white rounded-lg shadow px-6 py-4 mb-5">
                            <div className="div text-center mb-2">
                                <h1 className="text-xl font-bold mb-0">Invoice</h1>
                                <p className="text-base">{ noInvoice }</p>
                            </div>
                            <div className="md:flex justify-between mb-5">
                                <div className="md:w-3/4 mb-5">
                                    <h5 className="font-bold text-lg">Kepada</h5>
                                    <div className="mb-4">
                                        <h5>{customer.name || 'Nama Pelanggan'}</h5>
                                        <h5 className="text-sm">
                                            <FontAwesomeIcon icon={faEnvelope} fixedWidth className="mr-1 text-blue-600" />
                                            {customer.email}
                                        </h5>
                                        <h5 className="text-sm">
                                            <FontAwesomeIcon icon={faPhone} fixedWidth className="mr-1 text-blue-600" />
                                            {customer.phone_number}
                                        </h5>
                                    </div>
                                </div>
                                <div className="md:w-1/6">
                                    <div className="mb-2">
                                        <h5 className="font-bold text-lg">Diterbitkan</h5>
                                        <h5>{dateString(new Date(issuedDate))}</h5>
                                    </div>
                                    <div className="">
                                        <h5 className="font-bold text-lg">Jatuh Tempo</h5>
                                        <h5>{dateString(new Date(dueDate))}</h5>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div className="mb-4">
                                    <Table {...getTableProps()}>
                                        <TableHead>
                                            {headerGroups.map((headerGroup, idx) => (
                                                <TableHeadRow {...headerGroup.getHeaderGroupProps()} key={idx}>
                                                    {headerGroup.headers.map((column, idx) => (
                                                        <TableHeaderCell {...column.getHeaderProps()} key={idx}>
                                                            {column.render('Header')}
                                                        </TableHeaderCell>
                                                    ))}
                                                </TableHeadRow>
                                            ))}
                                        </TableHead>
                                        <TableBody {...getTableBodyProps()}>
                                            {// Loop over the table rows
                                                rows.map((row, idx) => {
                                                    // Prepare the row for display
                                                    prepareRow(row)
                                                    return (
                                                        // Apply the row props
                                                        <TableBodyRow className="hover:cursor-pointer" {...row.getRowProps()} key={idx}>
                                                            {// Loop over the rows cells
                                                                row.cells.map((cell, idx) => {
                                                                    // Apply the cell props
                                                                    return (
                                                                        <TableDataCell {...cell.getCellProps()} key={idx}>
                                                                            {// Render the cell contents
                                                                                cell.render('Cell')}
                                                                        </TableDataCell>
                                                                    )
                                                                })}
                                                        </TableBodyRow>
                                                    )
                                                })}
                                        </TableBody>
                                    </Table>
                                </div>

                                <div className="flex justify-end">
                                    <div className="rounded-lg overflow-auto border">
                                        <table className="">
                                            <tr className="border-b">
                                                <th className="border-r px-2 text-left">Total Normal</th>
                                                <td className="px-2">
                                                    <NumberFormat
                                                        thousandSeparator='.'
                                                        decimalSeparator=","
                                                        prefix='Rp. '
                                                        displayType="text"
                                                        value={productList.map(p => (p.qty * p.price)).reduce((prev, curr) => prev + curr, 0)}
                                                    />
                                                </td>
                                            </tr>
                                            <tr className="border-b">
                                                <th className="border-r px-2 text-left">Diskon</th>
                                                <td className="px-2">
                                                    <NumberFormat
                                                        thousandSeparator='.'
                                                        decimalSeparator=","
                                                        prefix='Rp. '
                                                        displayType="text"
                                                        value={productList.map(p => (p.qty * p.price) * ((parseFloat(p.discount)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                                    />
                                                </td>
                                            </tr>
                                            <tr className="border-b">
                                                <th className="border-r px-2 text-left">Pajak</th>
                                                <td className="px-2">
                                                    <NumberFormat
                                                        thousandSeparator='.'
                                                        decimalSeparator=","
                                                        prefix='Rp. '
                                                        displayType="text"
                                                        value={productList.map(p => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((parseFloat(p.tax)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                                    />
                                                </td>
                                            </tr>
                                            <tr className="border-b bg-blue-50">
                                                <th className="border-r px-2 text-left text-blue-600">Total</th>
                                                <td className="px-2">
                                                    <NumberFormat
                                                        thousandSeparator='.'
                                                        decimalSeparator=","
                                                        prefix='Rp. '
                                                        displayType="text"
                                                        value={productList.map(p => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((100 + parseFloat(p.tax)) / 100)).reduce((prev, curr) => prev + curr, 0)}
                                                    />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className="col-span-2">
                        <div className="bg-white rounded-lg shadow px-6 py-4 mb-5">
                            <div className="mb-5">
                                <h5 className="text-lg font-bold">Status Pembayaran</h5>
                                <Select options={paymentStatusOption}  onChange={(val) => {
                                    setPaymentStatus(val)
                                    updatePaymentStatus(val.value)
                                }} value={paymentStatus} />
                            </div>
                            <div className="mb-5">
                                <h5 className="text-lg font-bold">Opsi</h5>
                                <div className="grid grid-cols-2 gap-2">
                                    <Button
                                        className="py-1"
                                        onClick={() => PrintInvoice()}
                                    >
                                        <FontAwesomeIcon icon={faFilePdf} />
                                        <span className="ml-2">Download PDF</span>
                                    </Button>
                                    <Button
                                        className="py-1"
                                        onClick={() => {
                                            const {id} = router.query
                                            navigator.clipboard.writeText(webURL + `/share/invoice/${id}`)
                                            toast.success("Link berhasil disalin")
                                        }}
                                    >
                                        <FontAwesomeIcon icon={faLink} />
                                        <span className="ml-2">Copy Link</span>
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </Dashboard>
    )
}

export default CreateInvoice