import { faEdit, faEye, faTrash } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { useRouter } from "next/router"
import { useEffect, useMemo, useState } from "react"
import toast from "react-hot-toast"
import NumberFormat from "react-number-format"
import { useTable } from "react-table"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import { Table, TableBody, TableBodyRow, TableDataCell, TableHead, TableHeaderCell, TableHeadRow } from "../../../components/atoms/table"
import Header from "../../../components/molecules/header"
import Dashboard from "../../../components/templates/dashboard"
import { backendUrl } from "../../../helpers/constants"
import { dateString } from "../../../helpers/date"
import { JsonResponse } from "../../../helpers/type"

const InvoicePage = () => {
    const router = useRouter()
    const [invoices, setInvoices] = useState<Array<any>>([])

    useEffect(() => {
        const fetchData = async () => {
            const token = localStorage.getItem('token')

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/transaction/my', {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    setInvoices(res.data.data)
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    }
                } else {
                    alert(e)
                }
            }
        }

        fetchData()
    }, [])

    const data = useMemo((): any => {
        if (invoices) {
            let inv = [...invoices]
            inv.forEach(c => {
                c.name = c.customer.length > 0 ? c.customer[0].name : ''
                c.issued_date_string = dateString(new Date(c.issued_date))
                c.due_date_string = dateString(new Date(c.due_date))
                c.total = <NumberFormat
                    thousandSeparator='.'
                    decimalSeparator=","
                    prefix='Rp. '
                    displayType="text"
                    value={c.items.map((i: any) => i.total).reduce((prev: any, curr: any) => prev + curr, 0)}
                />
                c.action = (
                    <div className="flex gap-x-2">
                        <Button
                            link
                            variant="outline-success"
                            className="text-sm px-1"
                            title="Lihat"
                            href={router.pathname + "/detail/" + c.id}
                        // onClick={() => editCustomer(c)}
                        >
                            <FontAwesomeIcon icon={faEye} />
                        </Button>
                        <Button
                            link
                            variant="outline-primary"
                            className="text-sm px-1"
                            title="edit"
                            href={router.pathname + "/edit/" + c.id}
                        // onClick={() => editCustomer(c)}
                        >
                            <FontAwesomeIcon icon={faEdit} />
                        </Button>
                        <Button
                            variant="outline-danger"
                            className="text-sm px-1"
                            title="hapus"
                        // onClick={() => {
                        //     setCustomerId(c.id)
                        //     setOpenModal(true)
                        // }}
                        >
                            <FontAwesomeIcon icon={faTrash} />
                        </Button>
                    </div>
                )
            })

            return inv
        } else {
            return []
        }
    }, [invoices, router.pathname])

    const columns = useMemo(
        () => [
            {
                Header: 'Nomor',
                accessor: 'no_invoice_string', // accessor is the "key" in the data
            },
            {
                Header: 'Nama',
                accessor: 'name', // accessor is the "key" in the data
            },
            {
                Header: 'Diterbitkan',
                accessor: 'issued_date_string',
            },
            {
                Header: 'Jatuh Tempo',
                accessor: 'due_date_string',
            },
            {
                Header: 'Total',
                accessor: 'total',
            },
            {
                Header: '',
                accessor: 'action',
            },
        ],
        []
    )

    const tableInstance = useTable({ columns, data })
    console.log(data)

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = tableInstance

    return (
        <Dashboard>
            <Header
                title="Faktur"
                description="Faktur"
            />
            <Container className="pt-10" size="wide">
                <div className="flex items-center justify-between mb-5">
                    <h1 className="text-2xl font-bold">Daftar Invoice</h1>
                    <Button
                        link
                        variant="primary"
                        className="py-1"
                        href={router.pathname + '/create'}
                    >
                        Buat Invoice
                    </Button>
                </div>
                <div>
                    <Table {...getTableProps()}>
                        <TableHead>
                            {headerGroups.map((headerGroup, idx) => (
                                <TableHeadRow {...headerGroup.getHeaderGroupProps()} key={idx}>
                                    {headerGroup.headers.map((column, idx) => (
                                        <TableHeaderCell {...column.getHeaderProps()} key={idx}>
                                            {column.render('Header')}
                                        </TableHeaderCell>
                                    ))}
                                </TableHeadRow>
                            ))}
                        </TableHead>
                        <TableBody {...getTableBodyProps()}>
                            {// Loop over the table rows
                                rows.map((row, idx) => {
                                    // Prepare the row for display
                                    prepareRow(row)
                                    return (
                                        // Apply the row props
                                        <TableBodyRow className="hover:cursor-pointer" {...row.getRowProps()} key={idx}>
                                            {// Loop over the rows cells
                                                row.cells.map((cell, idx) => {
                                                    // Apply the cell props
                                                    return (
                                                        <TableDataCell {...cell.getCellProps()} key={idx}>
                                                            {// Render the cell contents
                                                                cell.render('Cell')}
                                                        </TableDataCell>
                                                    )
                                                })}
                                        </TableBodyRow>
                                    )
                                })}
                        </TableBody>
                    </Table>
                </div>
            </Container>
        </Dashboard>
    )
}

export default InvoicePage