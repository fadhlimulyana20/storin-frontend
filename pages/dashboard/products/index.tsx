import { faBarcode, faBoxOpen, faImage, faPencil, faPlus, faSearch, faStore, faTag, faTrash } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import Image from "next/image"
import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react"
import toast from "react-hot-toast"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import FormInput from "../../../components/atoms/input/formInput"
import Header from "../../../components/molecules/header"
import Modal from "../../../components/molecules/modal"
import Dashboard from "../../../components/templates/dashboard"
import { Context } from "../../../contexts/store"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"

const ProductsIndex = () => {
    const { globalState, dispatch } = useContext(Context)
    const [myProduct, setMyProduct] = useState<Array<any>>([])
    const [loadingMyProduct, setLoadingMyProduct] = useState(false)
    const [openModal, setOpenModal] = useState(false)
    const [productId, setProductId] = useState("")
    const router = useRouter()

    const deleteProduct = async (id: string) => {
        const token = localStorage.getItem("token")

        try {
            const res: AxiosResponse<JsonResponse> = await axios.delete(backendUrl + "/product/my/delete/" + id, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                setMyProduct([...myProduct.filter(p => p.id !== id)])
                setProductId("")
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                alert(e)
            }
        } finally {
            setOpenModal(false)
        }
    }

    useEffect(() => {
        const fetchMyProduct = async () => {
            const userId = globalState.myStore.store.id
            setLoadingMyProduct(true)

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/product/user/' + userId)

                if (res.status === 200) {
                    if (res.data.data) {
                        setMyProduct(res.data.data)
                    }
                }
            } catch (e: any) {
                if (e.response.data) {
                    toast.error(e.response.data.message)
                } else {
                    alert(e)
                }
            } finally {
                setLoadingMyProduct(false)
            }
        }

        if (globalState.myStore.store) {
            fetchMyProduct()
        }
    }, [globalState.auth.loggedUser, globalState.myStore.store])


    return (
        <Dashboard>
            <Header
                title="Produk"
                description="Produk"
            />
            <Modal
                open={openModal}
                setOpen={setOpenModal}
                type="question"
                title="Hapus Produk"
                onConfirm={() => deleteProduct(productId)}
            >
                Apakah anda yakin ingin menghapus produk ini?
            </Modal>
            <div className="fixed z-50">
                <div className="min-h-screen bg-gray-500 opacity-50">
                </div>
            </div>
            <Container className="pt-10" size="wide">
                <div className="flex justify-between items-center mb-5">
                    <h1 className="text-2xl font-bold">Produk</h1>
                    <div className="flex gap-x-2">
                        <Button
                            variant="primary"
                            className="py-1"
                            onClick={() => router.push(router.pathname + "/create")}
                        >
                            <FontAwesomeIcon icon={faPlus} className="mr-1" />
                            Tambah
                        </Button>
                        <Button
                            variant="primary"
                            className="py-1"
                            onClick={() => router.push("/s/" + globalState.myStore.store.nickname)}
                        >
                            <FontAwesomeIcon icon={faStore} className="mr-1" />
                            Katalog
                        </Button>
                    </div>

                </div>
                <div className="flex items-center mb-5">
                    <div className="w-64">
                        <FormInput
                            type="text"
                            name="search"
                            id="search"
                            placeholder="Cari Produk"
                        />
                    </div>
                    <Button
                        variant="primary"
                        className="py-2"
                        onClick={() => router.push(router.pathname + "/create")}
                    >
                        <FontAwesomeIcon icon={faSearch} />
                    </Button>
                </div>
                {loadingMyProduct ? (
                    <div>
                        <p>Loading...</p>
                    </div>
                ) : myProduct.map(p => (
                    <div className="p-3 rounded bg-white shadow mb-2" key={p.id}>
                        <div className="flex justify-between">
                            <div className="flex items-center gap-x-4">
                                <div>
                                    <div className="border rounded h-24 w-24 flex items-center justify-center">
                                        {p.path_image ? (
                                            <div className="rounded w-full">
                                                <Image src={p.path_image} alt='preview' layout='responsive' objectFit="contain" height={250} width={250} />
                                            </div>
                                        ) : (
                                            <FontAwesomeIcon icon={faImage} className='text-gray-500' />
                                        )}
                                    </div>
                                </div>
                                <div>
                                    <h5 className="font-bold">{p.name}</h5>
                                    <div className="flex gap-2">
                                        <h5 className="text-sm">
                                            <FontAwesomeIcon icon={faBarcode} fixedWidth={true} className="mr-1 text-blue-500" />
                                            <span>{p.sku}</span>
                                        </h5>
                                        <h5 className="text-sm">
                                            <FontAwesomeIcon icon={faBoxOpen} fixedWidth={true} className="mr-1 text-amber-700" />
                                            <span>{p.stock}</span>
                                        </h5>
                                        <h5 className="text-sm">
                                            <FontAwesomeIcon icon={faTag} fixedWidth={true} className="mr-1 text-green-500" />
                                            <span>{p.price}</span>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div className="flex gap-x-2">
                                <Button
                                    variant="outline-primary"
                                    className="px-1 h-max text-sm"
                                    onClick={() => router.push(router.pathname + "/edit/" + p.id)}
                                >
                                    <FontAwesomeIcon icon={faPencil} />
                                </Button>
                                <Button
                                    variant="outline-danger"
                                    className="px-1 h-max text-sm"
                                    onClick={() => {
                                        setProductId(p.id)
                                        setOpenModal(true)
                                    }}
                                >
                                    <FontAwesomeIcon icon={faTrash} />
                                </Button>
                            </div>
                        </div>
                    </div>
                ))}
            </Container>
        </Dashboard>
    )
}

export default ProductsIndex