import { faImage, faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { Formik } from "formik"
import Image from "next/image"
import { useRouter } from "next/router"
import { useContext, useEffect, useRef, useState } from "react"
import toast from "react-hot-toast"
import Button from "../../../../components/atoms/button"
import Container from "../../../../components/atoms/container"
import FormError from "../../../../components/atoms/input/formError"
import FormInput from "../../../../components/atoms/input/formInput"
import FormLabel from "../../../../components/atoms/input/formLabel"
import TextArea from "../../../../components/atoms/input/textArea"
import Header from "../../../../components/molecules/header"
import TitleBackButton from "../../../../components/molecules/title/titleBackButton"
import Dashboard from "../../../../components/templates/dashboard"
import { Context } from "../../../../contexts/store"
import { isFileImage } from "../../../../helpers/checkTypes"
import { backendUrl } from "../../../../helpers/constants"
import { hasWhiteSpace } from "../../../../helpers/string"
import { JsonResponse } from "../../../../helpers/type"
import { uploadGlobal } from "../../../../helpers/upload"

const EditPorductPage = () => {
    const { globalState, dispatch } = useContext(Context)
    const [product, setProduct] = useState<any>()
    const [mainImage, setMainImage] = useState<File | null>(null)
    const [mainImageSrc, setMainImageSrc] = useState<string | ArrayBuffer | null>(null)
    const refMainImgageInput = useRef<HTMLInputElement>(null)
    const router = useRouter()
    const { id } = router.query

    const ChangeImage = (e: any) => {
        if (e.target.files) {
            const file: File = e.target.files[0]
            if (file) {
                if (file.size >= 1024000) {
                    toast.error("Ukuran file terlalu besar")
                } else if (!isFileImage(file)) {
                    toast.error("File harus berupa gambar")
                } else {
                    setMainImage(file)

                    const reader = new FileReader()
                    const url = reader.readAsDataURL(e.target.files[0])

                    reader.onloadend = function (e) {
                        setMainImageSrc(reader.result)
                    }
                }
            }
        }
    }

    useEffect(() => {
        const fetchProduct = async () => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/product/" + id)

                if (res.status === 200) {
                    setProduct(res.data.data)
                    if (res.data.data.path_image) {
                        setMainImageSrc(res.data.data.path_image)
                    }
                }
            } catch (e: any) {
                if (e.response.data) {
                    toast.error(e.response.data.message)
                } else {
                    alert(e)
                }
            }
        }

        if (id) {
            fetchProduct()
        }
    }, [id])


    const saveProduct = async ({ name, description, price, stock, sku }: any) => {
        const token = localStorage.getItem('token')

        try {
            let resImage = null
            if (mainImage && token) {
                resImage = await uploadGlobal(mainImage, "product/image", token)
            } 

            const data =  resImage ? {
                sku,
                name,
                description,
                price,
                stock,
                path_image: resImage ? resImage.data.file_path : ''
            } : {
                sku,
                name,
                description,
                price,
                stock,
            }
            
            const res: AxiosResponse<JsonResponse> = await axios.put(backendUrl + "/product/my/update/" + id, data, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                toast.success("Produk berhasil diperbarui")
                router.back()
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                alert(e)
            }
        }
    }

    return (
        <Dashboard>
            <Header
                title="Buat Produk"
                description="Buat Produk"
            />
            <Container className="pt-10" size="wide">
                <div className="mb-5">
                    <TitleBackButton title="Ubah Produk" />
                </div>
                <div className="bg-white shadow p-4 rounded">
                    <Formik
                        enableReinitialize
                        initialValues={
                            {
                                sku: product ? product.sku : '',
                                name: product ? product.name : '',
                                description: product ? product.description : '',
                                price: product ? product.price : '',
                                stock: product ? product.stock : '',
                            }
                        }
                        validate={(values => {
                            const errors: any = {}

                            if (!values.sku) {
                                errors.sku = 'SKU harus diisi'
                            }

                            if (hasWhiteSpace(values.sku)) {
                                errors.sku = 'SKU tidak boleh mengandung spasi'
                            }

                            if (!values.name) {
                                errors.name = 'Nama harus diisi'
                            }

                            if (!values.price) {
                                errors.price = 'Harga harus diisi'
                            }

                            if (!values.stock) {
                                errors.stock = 'Stok harus diisi'
                            }

                            if (!values.description) {
                                errors.description = 'Deskripsi harus diisi'
                            }

                            return errors;
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setSubmitting(true)
                            saveProduct(values)
                                .then(r => {
                                    setSubmitting(false)
                                })
                        }}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                        }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="mb-5">
                                    <FormLabel htmlFor="sku">SKU Produk</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="ABC01"
                                        id="sku"
                                        name="sku"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.sku}
                                    />
                                    <FormError>
                                        {errors.sku && touched.sku && errors.sku}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="nama">Nama Produk</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="Kemeja Putih"
                                        id="name"
                                        name="name"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.name}
                                    />
                                    <FormError>
                                        {errors.name && touched.name && errors.name}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="nama">Stok</FormLabel>
                                    <FormInput
                                        type={"number"}
                                        placeholder="Stok"
                                        id="stock"
                                        name="stock"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.stock}
                                    />
                                    <FormError>
                                        {errors.stock && touched.stock && errors.stock}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="price">Harga</FormLabel>
                                    <FormInput
                                        type={"number"}
                                        placeholder="Harga"
                                        id="price"
                                        name="price"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.price}
                                    />
                                    <FormError>
                                        {errors.price && touched.price && errors.price}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="description">Deskripsi</FormLabel>
                                    <TextArea
                                        placeholder="Deskripsi"
                                        id="description"
                                        name="description"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.description}
                                        rows={5}
                                    />
                                    <FormError>
                                        {errors.description && touched.description && errors.description}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="nama">Logo</FormLabel>
                                    <div className="rounded border p-3 flex items-center justify-center">
                                        <div className="flex flex-col">
                                            <input type="file" className='hidden' ref={refMainImgageInput} onChange={ChangeImage} />
                                            {typeof mainImageSrc === "string" ? (
                                                <div className="border p-3 rounded w-72 mb-2">
                                                    <Image src={mainImageSrc} alt='preview' layout='responsive' objectFit="contain" height={250} width={250} />
                                                </div>
                                            ) : (
                                                <FontAwesomeIcon icon={faImage} className="text-3xl mb-2 text-gray-500" />
                                            )}
                                            <Button
                                                variant="outline-primary"
                                                onClick={() => refMainImgageInput.current?.click()}
                                            >
                                                Pilih File
                                            </Button>
                                        </div>
                                    </div>
                                </div>

                                <div className="flex justify-end">
                                    <Button variant="primary" className="w-full md:w-32 py-2 " type="submit" disabled={isSubmitting}>
                                        {isSubmitting ? 'Menyimpan...' : (
                                            <div>
                                                <FontAwesomeIcon icon={faSave} className="mr-2" />
                                                Simpan
                                            </div>
                                        )}
                                    </Button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>

            </Container>
        </Dashboard>
    )
}

export default EditPorductPage