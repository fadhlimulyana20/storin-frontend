import { faArrowLeft, faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { Formik } from "formik"
import { useContext } from "react"
import toast from "react-hot-toast"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import FormError from "../../../components/atoms/input/formError"
import FormInput from "../../../components/atoms/input/formInput"
import FormLabel from "../../../components/atoms/input/formLabel"
import Header from "../../../components/molecules/header"
import TitleBackButton from "../../../components/molecules/title/titleBackButton"
import Dashboard from "../../../components/templates/dashboard"
import { Context } from "../../../contexts/store"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"

const ProfileSetting = () => {
    const { globalState, dispatch } = useContext(Context)
    const { loggedUser } = globalState.auth

    const updateProile = async (name: string, username: string, email: string, no_telepon: string) => {
        const token = localStorage.getItem('token')
        try {
            const res: AxiosResponse<JsonResponse> = await axios.put(backendUrl + '/auth/update/user', {
                ...loggedUser,
                name,
                username,
                email,
                no_telepon
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                toast.success("Profil berhasil diperbarui")
            }
        } catch(e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            }else {
                alert (e)
            }
        }
    }

    return (
        <Dashboard>
            <Header
                title="Pengaturan Akun"
                description="Mengubah pengaturan akun storin"
            />
            <Container size="wide" className="pt-10">
                <div className="mb-5">
                    <TitleBackButton title="Pengaturan Profil" />
                </div>
                <div className="bg-white shadow p-4 rounded">
                    <Formik
                        enableReinitialize
                        initialValues={
                            { 
                                name: loggedUser ? loggedUser.name : '', 
                                username: loggedUser ? loggedUser.username : '', 
                                email: loggedUser ? loggedUser.email : '', 
                                no_telepon: loggedUser ? loggedUser.no_telepon : '', 
                            }
                        }
                        validate={(values => {
                            const errors: any = {}
                            if (!values.email) {
                                errors.email = 'Email harus diisi'
                            } else if (
                                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                            ) {
                                errors.email = 'Email tidak valid';
                            }

                            if (!values.username) {
                                errors.username = 'Username harus diisi'
                            }

                            if (!values.name) {
                                errors.name = 'Nama harus diisi'
                            }

                            // if (!values.no_telepon) {
                            //     errors.no_telepon = 'No. Telepon harus diisi'
                            // }

                            return errors;
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setSubmitting(true)
                            updateProile(values.name, values.username, values.email, values.no_telepon)
                                .then(r => {
                                    setSubmitting(false)
                                })
                        }}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                        }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="mb-5">
                                    <FormLabel htmlFor="nama">Nama Lengkap</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="Masukan Nama Lengkap"
                                        id="name"
                                        name="name"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.name}
                                    />
                                    <FormError>
                                        {errors.name && touched.name && errors.name}
                                    </FormError>
                                </div>
                                <div className="mb-5">
                                    <FormLabel htmlFor="username">Username</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="Buat Username"
                                        id="username"
                                        name="username"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.username}
                                    />
                                    <FormError>
                                        {errors.username && touched.username && errors.username}
                                    </FormError>
                                </div>
                                <div className="mb-5">
                                    <FormLabel htmlFor="email">Email</FormLabel>
                                    <FormInput
                                        type={"email"}
                                        placeholder="Masukan Email"
                                        id="email"
                                        name="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                    />
                                    <FormError>
                                        {errors.email && touched.email && errors.email}
                                    </FormError>
                                </div>
                                <div className="mb-5">
                                    <FormLabel htmlFor="no_telepon">No Telepon</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="Masukan No Telepon"
                                        id="no_telepon"
                                        name="no_telepon"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.no_telepon}
                                    />
                                    <FormError>
                                        {errors.no_telepon && touched.no_telepon && errors.no_telepon}
                                    </FormError>
                                </div>
                                <div className="flex justify-end">
                                    <Button variant="primary" className="w-full md:w-32 py-2 mb-4" type="submit" disabled={isSubmitting}>
                                        {isSubmitting ? 'Menyimpan...' : (
                                            <div>
                                                <FontAwesomeIcon icon={faSave} className="mr-2" />
                                                Simpan
                                            </div>
                                        )}
                                    </Button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>
            </Container>
        </Dashboard>
    )
}


export default ProfileSetting