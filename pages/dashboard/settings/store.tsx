import { faImage, faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { Formik } from "formik"
import Image from "next/image"
import { useContext, useEffect, useRef, useState } from "react"
import toast from "react-hot-toast"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import FormError from "../../../components/atoms/input/formError"
import FormInput from "../../../components/atoms/input/formInput"
import FormLabel from "../../../components/atoms/input/formLabel"
import Header from "../../../components/molecules/header"
import TitleBackButton from "../../../components/molecules/title/titleBackButton"
import Dashboard from "../../../components/templates/dashboard"
import { Context } from "../../../contexts/store"
import { isFileImage } from "../../../helpers/checkTypes"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"
import { uploadGlobal } from "../../../helpers/upload"

const StoreSetting = () => {
    const [loadingMyStore, setLoadingMyStore] = useState(false)
    const [myStore, setMyStore] = useState<any>(null)
    const { globalState, dispatch } = useContext(Context)
    const [logoImage, setLogoImage] = useState<File | any>()
    const [logoImageSrc, setLogoImageSrc] = useState<string | ArrayBuffer | null>()
    const refLogoImageInput = useRef<HTMLInputElement>(null)

    const ChangeImage = (e: any) => {
        if (e.target.files) {
            const file: File = e.target.files[0]
            if (file) {
                if (file.size >= 1024000) {
                    toast.error("Ukuran file terlalu besar")
                } else if (!isFileImage(file)) {
                    toast.error("File harus berupa gambar")
                } else {
                    setLogoImage(file)

                    const reader = new FileReader()
                    const url = reader.readAsDataURL(e.target.files[0])

                    reader.onloadend = function (e) {
                        setLogoImageSrc(reader.result)
                    }
                }
            }
        }
    }

    const updateStore = async (name: string, nickname: string) => {
        const token = localStorage.getItem('token')
        const storeId = myStore !== null ? myStore.id : ''

        try {
            let resImage = null
            if (logoImage && token) {
                resImage = await uploadGlobal(logoImage, `store/${storeId}/logo`, token)
            }

            const data = resImage ? {
                name,
                nickname,
                path_logo: resImage ? resImage.data.file_path : ''
            } : {
                name,
                nickname
            }

            if (storeId !== '') {
                const res: AxiosResponse<JsonResponse> = await axios.put(backendUrl + "/store/my/update/" + storeId, data, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    toast.success("Toko berhasil diperbarui")
                }
            } else {
                const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + "/store/my/create", data, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    toast.success("Toko berhasil diperbarui")
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                alert(e)
            }
        }
    }

    useEffect(() => {
        const fetchMyStore = async () => {
            const userId = globalState.auth.loggedUser.id
            setLoadingMyStore(true)

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/user/" + userId)

                if (res.status === 200) {
                    setMyStore(res.data.data)
                    if (res.data.data.path_logo) {
                        setLogoImageSrc(res.data.data.path_logo)
                    }
                }
            } catch (e: any) {
                console.log(e)
            } finally {
                setLoadingMyStore(false)
            }
        }

        if (globalState.auth.loggedUser) {
            fetchMyStore()
        }
    }, [globalState.auth.loggedUser, globalState.auth.loggedUser?.id])

    return (
        <Dashboard>
            <Header
                title="Pengaturan Toko"
                description="Mengubah pengaturan Toko storin"
            />
            <Container size="wide" className="pt-10">
                <div className="mb-5">
                    <TitleBackButton title="Pengaturan Toko" />
                </div>
                <div className="bg-white shadow p-4 rounded">
                    <h1 className="text-lg font-bold mb-4">Identitas Toko</h1>
                    <Formik
                        enableReinitialize
                        initialValues={
                            {
                                name: myStore !== null ? myStore.name : '',
                                nickname: myStore !== null ? myStore.nickname : ''
                            }
                        }
                        validate={(values => {
                            const errors: any = {}

                            if (!values.name) {
                                errors.name = 'Nama harus diisi'
                            }

                            return errors;
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setSubmitting(true)
                            updateStore(values.name, values.nickname)
                                .then(r => {
                                    setSubmitting(false)
                                })
                        }}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                        }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="mb-5">
                                    <FormLabel htmlFor="nama">Nama Toko</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="Toko ABC"
                                        id="name"
                                        name="name"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.name}
                                    />
                                    <FormError>
                                        {errors.name && touched.name && errors.name}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="nama">Nickname Toko</FormLabel>
                                    <FormInput
                                        type={"text"}
                                        placeholder="tokoabc"
                                        id="nickname"
                                        name="nickname"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.nickname}
                                    />
                                    <FormError>
                                        {errors.nickname && touched.nickname && errors.nickname}
                                    </FormError>
                                </div>

                                <div className="mb-5">
                                    <FormLabel htmlFor="nama">Logo</FormLabel>
                                    <div className="rounded border p-3 flex items-center justify-center">
                                        <div className="flex flex-col">
                                            <input type="file" className='hidden' ref={refLogoImageInput} onChange={ChangeImage} />
                                            {typeof logoImageSrc === "string" ? (
                                                <div className="border p-3 rounded w-72 mb-2">
                                                    <Image src={logoImageSrc} alt='preview' layout='responsive' objectFit="contain" height={250} width={250} />
                                                </div>
                                            ) : (
                                                <div className="text-center">
                                                    <FontAwesomeIcon icon={faImage} className="text-3xl mb-2 text-gray-500" />
                                                    <p className="text-sm text-center mb-2 text-gray-700">Unggah file gambar persegi dengan ukuran 256x256 px.</p>
                                                </div>
                                            )}
                                            <Button
                                                variant="outline-primary"
                                                onClick={() => refLogoImageInput.current?.click()}
                                            >
                                                Pilih File
                                            </Button>
                                        </div>
                                    </div>
                                </div>

                                <div className="flex justify-end">
                                    <Button variant="primary" className="w-full md:w-32 py-2 " type="submit" disabled={isSubmitting}>
                                        {isSubmitting ? 'Menyimpan...' : (
                                            <div>
                                                <FontAwesomeIcon icon={faSave} className="mr-2" />
                                                Simpan
                                            </div>
                                        )}
                                    </Button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>
            </Container>
        </Dashboard>
    )
}

export default StoreSetting