import { faLock, faReceipt, faStore, faUserCog } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useRouter } from "next/router"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import Header from "../../../components/molecules/header"
import Dashboard from "../../../components/templates/dashboard"

const DashoardIndex = () => {
    const router = useRouter()

    const menu = [
        {
            name: "profile",
            display: "Profil",
            href: "/profile",
            icon: faUserCog
        },
        {
            name: "password",
            display: "Password",
            href: "/password",
            icon: faLock
        },
        {
            name: "store",
            display: "Toko",
            href: "/store",
            icon: faStore
        },
        {
            name: "invoice",
            display: "Invoice",
            href: "/invoice",
            icon: faReceipt
        }
    ]

    return (
        <Dashboard>
            <Header
                title="Pengaturan"
                description="Pengaturan aplikasi storin"
            />
            <Container className="pt-10" size="wide">
                <h1 className="text-2xl font-bold mb-5">Pengaturan</h1>
                { menu.map((m, idx) => (
                    <div key={idx} className="mb-2">
                        <Button 
                            variant="white"
                            className="w-full px-4 py-3 text-left font-semibold shadow"
                            type="button"
                            onClick={ () => router.push(router.pathname + m.href) }
                        >
                            <FontAwesomeIcon icon={m.icon} className="mr-2 text-blue-500" fixedWidth={true} />
                            {m.display}
                        </Button>
                    </div>
                )) }
            </Container>
        </Dashboard>
    )
}


export default DashoardIndex