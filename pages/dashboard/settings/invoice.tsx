import { faSave } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Formik } from "formik";
import Button from "../../../components/atoms/button";
import Container from "../../../components/atoms/container";
import FormError from "../../../components/atoms/input/formError";
import FormInput from "../../../components/atoms/input/formInput";
import FormLabel from "../../../components/atoms/input/formLabel";
import Header from "../../../components/molecules/header";
import TitleBackButton from "../../../components/molecules/title/titleBackButton";
import Dashboard from "../../../components/templates/dashboard";
// import { Editor } from '@tinymce/tinymce-react';
import { useContext, useEffect, useRef, useState } from "react";
import { EditorState, ContentState, convertToRaw, convertFromHTML } from 'draft-js';
// import { Editor } from 'react-draft-wysiwyg';
import dynamic from 'next/dynamic';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { convertToHTML } from 'draft-convert'
import axios, { Axios, AxiosResponse } from "axios";
import { JsonResponse } from "../../../helpers/type";
import { backendUrl } from "../../../helpers/constants";
import { Context } from "../../../contexts/store";
import toast from "react-hot-toast";
import Image from "next/image";
const Editor: any = dynamic(
    (): any => import('react-draft-wysiwyg').then(mod => mod.Editor),
    { ssr: false }
)

export default function InvoiceSetting() {
    const [note, setNote] = useState(EditorState.createEmpty())
    const [config, setConfig] = useState<any>()
    const { globalState, dispatch } = useContext(Context)

    useEffect(() => {
        const fetchInvoiceConfig = async () => {
            const token = localStorage.getItem('token')
            const { myStore } = globalState
    
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/v1/config/invoice/store/" + myStore.store.id, {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })
    
                setConfig(res.data.data)
            } catch(e: any){
                if (e.response.data) {
                    // toast.error(e.response.data.message)
                }else {
                    alert (e)
                }
            } 
        }

        if (globalState.auth.isUserAuthenticated && globalState.myStore.store) {
            fetchInvoiceConfig()
        }

    }, [globalState])

    useEffect(() => {
        if (config) {
            const content = convertFromHTML(config.note)
            setNote(EditorState.createWithContent(ContentState.createFromBlockArray(content.contentBlocks, content.entityMap)))
        }
    }, [config])
    
    const saveInvoiceConfig = async () => {
        const token = localStorage.getItem('token')

        if (config) {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.put(backendUrl + `/v1/config/invoice/update/${config ? config.id : ''}`, {
                    ...config,
                    note: convertToHTML(note.getCurrentContent())
                 },
                 {
                     headers: {
                         Authorization: `Bearer ${token}`
                     }
                 })
    
                if (res.status === 200) {
                    toast.success("Pengaturan invoice berhasil disimpan")
                }
    
            } catch(e: any){
                if (e.response.data) {
                    toast.error(e.response.data.message)
                }else {
                    alert (e)
                }

            }
        } else {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + "/v1/config/invoice/create", {
                    ...config,
                    note: convertToHTML(note.getCurrentContent())
                }, 
                {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })
    
                if (res.status === 200) {
                    toast.success("Pengaturan invoice berhasil disimpan")
                }
    
            } catch(e: any){
                if (e.response.data) {
                    toast.error(e.response.data.message)
                }else {
                    alert (e)
                }

            }
        }
    }

    return (
        <Dashboard>
            <Header
                title="Pengaturan Invoice"
                description="Mengubah pengaturan invoice storin"
            />
            <Container size="wide" className="pt-10">
                <div className="mb-5">
                    <TitleBackButton title="Pengaturan Invoice" />
                </div>
                <div className="bg-white shadow p-4 rounded">
                    <Formik
                        enableReinitialize
                        initialValues={
                            {
                                note: EditorState.createEmpty()
                            }
                        }
                        validate={(values => {
                            const errors: any = {}


                            return errors;
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setSubmitting(true)
                            saveInvoiceConfig().then(r => {
                                setSubmitting(false)
                            })
                        }}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                        }) => (
                            <form onSubmit={handleSubmit}>
                                <div className="mb-5">
                                    <h1>{process.env.PUBLIC_URL}</h1>
                                    <FormLabel htmlFor="new_password_confirm">Catatan</FormLabel>
                                    {typeof window !== 'undefined' && (
                                        <div className="border border-gray-200 focus:border-blue-700 rounded focus:outline-none focus:border-blue-500 focus:ring-1 p-3">
                                            <Editor
                                                wrapperClassName="wrapper-class"
                                                editorClassName="editor-class"
                                                toolbarClassName="toolbar-class"
                                                toolbar={{
                                                    options: ['inline', 'blockType', 'list', 'textAlign', 'history'],
                                                    inline: { inDropdown: true },
                                                    list: { inDropdown: true },
                                                    link: { inDropdown: true },
                                                    history: { inDropdown: true },
                                                }}
                                                editorState={note}
                                                onEditorStateChange={(editorState: any) => {
                                                    setNote(editorState)
                                                }}
                                            />

                                        </div>
                                    )}
                                    <p className="text-sm text-blue-500 font-medium">*Tambahkan catatan untuk inovice anda, seperti syarat dan ketentuan atau aturan pembelian.</p>
                                    <FormError>
                                        {/* {errors.new_password_confirm && touched.new_password_confirm && errors.new_password_confirm} */}
                                    </FormError>
                                </div>
                                <div className="mb-5">
                                    <FormLabel>Tema Invoice</FormLabel>
                                    <div className="flex gap-4 border rounded p-4">
                                        <div className="border rounded p-2 hover:bg-gray-50 hover:cursor-pointer border-blue-100 ring-2 ring-sky-200 text-center">
                                            <Image src="/images/invoice_theme/default.png" alt="storin-theme-default" layout="fixed" width={200} height={200} />
                                            <p className="text-sm text-blue-800">default</p>
                                        </div>
                                        <div className="border rounded p-2 flex items-center">
                                            <p>Segera tersedia tema lainnya.</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex justify-end">
                                    <Button variant="primary" className="w-full md:w-32 py-2 mb-4" type="submit" disabled={isSubmitting}>
                                        {isSubmitting ? 'Menyimpan...' : (
                                            <div>
                                                <FontAwesomeIcon icon={faSave} className="mr-2" />
                                                Simpan
                                            </div>
                                        )}
                                    </Button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>
            </Container>
        </Dashboard>
    )
}