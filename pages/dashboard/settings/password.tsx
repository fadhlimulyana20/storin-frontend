import { faArrowLeft, faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { Formik } from "formik"
import { useContext } from "react"
import toast from "react-hot-toast"
import Button from "../../../components/atoms/button"
import Container from "../../../components/atoms/container"
import FormError from "../../../components/atoms/input/formError"
import FormInput from "../../../components/atoms/input/formInput"
import FormLabel from "../../../components/atoms/input/formLabel"
import Header from "../../../components/molecules/header"
import TitleBackButton from "../../../components/molecules/title/titleBackButton"
import Dashboard from "../../../components/templates/dashboard"
import { Context } from "../../../contexts/store"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"

const PasswordSetting = () => {
    const { globalState, dispatch } = useContext(Context)
    const { loggedUser } = globalState.auth

    const updatePassword = async (old_password: string, new_password: string, new_password_confirm: string) => {
        const token = localStorage.getItem('token')
        try {
            const res: AxiosResponse<JsonResponse> = await axios.put(backendUrl + '/auth/update/password', {
                old_password,
                new_password,
                new_password_confirm
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                toast.success("Password berhasil diperbarui")
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                alert(e)
            }
        }
    }

    return (
        <Dashboard>
            <Header
                title="Ubah Password"
                description="Mengubah password akun storin"
            />
            <Container size="wide" className="pt-10">
                <div className="mb-5">
                    <TitleBackButton title={ globalState.auth.loggedUser?.is_password_default ? "Buat Password" : "Ubah Password" } />
                </div>
                <div className="bg-white shadow p-4 rounded">
                    <Formik
                        enableReinitialize
                        initialValues={
                            {
                                old_password: '',
                                new_password: '',
                                new_password_confirm: ''
                            }
                        }
                        validate={(values => {
                            const errors: any = {}

                            if (!globalState.auth.loggedUser?.is_password_default) {
                                if (!values.old_password) {
                                    errors.old_password = 'Password harus diisi'
                                }
                            }

                            if (values.new_password.length < 8) {
                                errors.new_password = 'Panjang password minimal 8 karakter'
                            }

                            if (values.new_password_confirm != values.new_password) {
                                errors.new_password_confirm = 'Password tidak sama'
                            }

                            return errors;
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setSubmitting(true)
                            updatePassword(values.old_password, values.new_password, values.new_password_confirm)
                                .then(r => {
                                    setSubmitting(false)
                                })
                        }}
                    >
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                        }) => (
                            <form onSubmit={handleSubmit}>
                                { globalState.auth.loggedUser?.is_password_default && (
                                    <p className="mb-5">Buat password baru agar kamu bisa login mengggunakan email dan password biasa.</p>
                                ) }
                                { !globalState.auth.loggedUser?.is_password_default && (
                                    <div className="mb-5">
                                        <FormLabel htmlFor="old_password">Password Lama</FormLabel>
                                        <FormInput
                                            type={"password"}
                                            placeholder="Masukan Password Lama"
                                            id="old_password"
                                            name="old_password"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            value={values.old_password}
                                        />
                                        <FormError>
                                            {errors.old_password && touched.old_password && errors.old_password}
                                        </FormError>
                                    </div>
                                ) }
                                <div className="mb-5">
                                    <FormLabel htmlFor="new_password">Password Baru</FormLabel>
                                    <FormInput
                                        type={"password"}
                                        placeholder="Masukan Password Baru"
                                        id="new_password"
                                        name="new_password"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.new_password}
                                    />
                                    <FormError>
                                        {errors.new_password && touched.new_password && errors.new_password}
                                    </FormError>
                                </div>
                                <div className="mb-5">
                                    <FormLabel htmlFor="new_password_confirm">Konfirmasi Password Baru</FormLabel>
                                    <FormInput
                                        type={"password"}
                                        placeholder="Masukan Password Baru"
                                        id="new_password_confirm"
                                        name="new_password_confirm"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.new_password_confirm}
                                    />
                                    <FormError>
                                        {errors.new_password_confirm && touched.new_password_confirm && errors.new_password_confirm}
                                    </FormError>
                                </div>
                                <div className="flex justify-end">
                                    <Button variant="primary" className="w-full md:w-32 py-2 mb-4" type="submit" disabled={isSubmitting}>
                                        {isSubmitting ? 'Menyimpan...' : (
                                            <div>
                                                <FontAwesomeIcon icon={faSave} className="mr-2" />
                                                Simpan
                                            </div>
                                        )}
                                    </Button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>
            </Container>
        </Dashboard>
    )
}


export default PasswordSetting