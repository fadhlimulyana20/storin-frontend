import { faInfo, faInfoCircle, faStore } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react"
import { Doughnut } from "react-chartjs-2"
import toast from "react-hot-toast"
import NumberFormat from "react-number-format"
import Button from "../../components/atoms/button"
import Container from "../../components/atoms/container"
import Header from "../../components/molecules/header"
import Dashboard from "../../components/templates/dashboard"
import { Context } from "../../contexts/store"
import { backendUrl } from "../../helpers/constants"
import { JsonResponse } from "../../helpers/type"
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';   
import { Transition } from "@headlessui/react"
import Image from "next/image"

ChartJS.register(ArcElement, Tooltip, Legend);

const DashoardIndex = () => {
    const [loadingMyStore, setLoadingMyStore] = useState(false)
    const [myStore, setMyStore] = useState<any>(null)
    const { globalState, dispatch } = useContext(Context)
    const router = useRouter()
    const [paidTotal, setPaidTotal] = useState(0)
    const [unpaidTotal, setUnpaidTotal] = useState(0)
    const [paidThisMonth, setPaidThisMonth] = useState(0)
    const [countPaid, setCountPaid] = useState(0)
    const [countUnpaid, setCountUnpaid] = useState(0) 
    const [countCustomer, setCountCustomer] = useState(0) 
    const [countCustomerTransaction, setCountCustomerTransaction] = useState(0)

    const fetchPaidSumTotal = async () => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios.get(backendUrl + "/v1/transaction/my/paid/total", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                if (res.data.data) {
                    setPaidTotal(res.data.data[0].sum)
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                console.log(e)
            }
        }
    }

    const fetchUnpaidSumTotal = async () => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios.get(backendUrl + "/v1/transaction/my/unpaid/total", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                if (res.data.data) {
                    setPaidTotal(res.data.data[0].sum)
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                console.log(e)
            }
        }
    }

    const fetchPaidSumThisMonth = async () => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios.get(backendUrl + "/v1/transaction/my/paid/current-month", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                if (res.data.data) {
                    // console.log(res.data.data[0])
                    setPaidThisMonth(res.data.data[0].sum)
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                console.log(e)
            }
        }
    }

    const fetchCountPaid = async () => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios.get(backendUrl + "/v1/transaction/my/paid/count", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                if (res.data.data) {
                    // console.log(res.data.data[0])
                    setCountPaid(res.data.data)
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                console.log(e)
            }
        }
    }

    const fetchCountUnpaid = async () => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios.get(backendUrl + "/v1/transaction/my/unpaid/count", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                if (res.data.data) {
                    // console.log(res.data.data[0])
                    setCountUnpaid(res.data.data)
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                console.log(e)
            }
        }
    }

    const fetchCountCustomer = async () => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios.get(backendUrl + "/v1/customer/my/count", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                if (res.data.data) {
                    // console.log(res.data.data[0])
                    setCountCustomer(res.data.data)
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                console.log(e)
            }
        }
    }

    const fetchCountCustomerTransaction = async () => {
        const token = localStorage.getItem('token')

        try {
            const res = await axios.get(backendUrl + "/v1/transaction/my/customer/count", {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })

            if (res.status === 200) {
                if (res.data.data) {
                    // console.log(res.data.data[0])
                    setCountCustomerTransaction(res.data.data)
                }
            }
        } catch (e: any) {
            if (e.response.data) {
                toast.error(e.response.data.message)
            } else {
                console.log(e)
            }
        }
    }

    useEffect(() => {
        fetchPaidSumTotal()
        fetchPaidSumThisMonth()
        fetchUnpaidSumTotal()
        fetchCountPaid()
        fetchCountUnpaid()
        fetchCountCustomer()
        fetchCountCustomerTransaction()
    }, [])

    useEffect(() => {
        const fetchMyStore = async () => {
            const userId = globalState.auth.loggedUser.id
            setLoadingMyStore(true)

            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/user/" + userId)

                if (res.status === 200) {
                    setMyStore(res.data.data)
                }
            } catch (e: any) {
                router.push("/registration/create-store")
            } finally {
                setLoadingMyStore(false)
            }
        }

        if (globalState.auth.loggedUser) {
            fetchMyStore()
        }
    }, [globalState.auth.loggedUser, globalState.auth.loggedUser?.id])

    const optionDoughnut = {
        layout: {
            padding: 0
        }
    }
    const dataDoughnut = {
        labels: ['Paid', 'Unpaid'],
        datasets: [
          {
            label: '# of Votes',
            data: [countPaid, countUnpaid],
            backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)',
            ],
            borderWidth: 1,
          },
        ],
      };

    return (
        <Dashboard>
            <Header
                title="Beranda"
                description="Kelola Produk dan Penjualanmu dari Satu Tempat dengan Mudah. Beralih ke pencatatan penjualan digital, buat invoice, kelola produk, dan lihat laporan bulanan lewat Storin."
            />
            <Container size="wide">
                <div className="pt-8">
                    <div className="md:grid md:grid-cols-3 gap-x-4">
                        <div className="bg-white shadow rounded px-10 pt-6 mb-5 col-span-2">
                            <h1 className="text-2xl font-bold">Halo, { globalState.auth.loggedUser ? globalState.auth.loggedUser.name : '' } !</h1>
                            <Image src="/images/undraw_handcrafts_analytics.svg" alt="storin" layout="fixed" height={250} width={250} />
                        </div>
                        <div className="bg-blue-100 shadow rounded p-5 mb-5">
                            <h1 className="text-2xl text-blue-900 font-bold mb-4">
                                <FontAwesomeIcon icon={faInfoCircle} />
                                <span className="ml-2">Informasi</span>
                            </h1>
                            <div className="py-2 text-blue-900 text-base space-y-2">
                                <p>Selamat datang di storin.id, aplikasi invoicing digital ringan yang memudahkan pengguna untuk membuat dan mengatur invoice penjualan mereka.</p>
                                <p>Storin.id dirancang untuk selalu ringan agar bisa cepat untuk diakses untuk menghemat waktu pengguna.</p>
                            </div>
                        </div>
                    </div>
                    <div className="md:grid md:grid-cols-5 gap-x-4">
                        <div className="bg-white rounded shadow p-5 mb-5 col-span-3">
                            <h5 className="text-xl font-bold mb-5">Ikhtisar</h5>
                            <div className="mb-8"> 
                                <h5 className="text-gray-400 font-bold uppercase text-xs">Total Pemasukan</h5>
                                <NumberFormat thousandSeparator='.' decimalSeparator="," prefix="Rp. " displayType="text" value={paidTotal} className="text-3xl font-black mb-2" />
                                {/* <h5 className="text-gray-600 text-xs font-semibold">100 Faktur</h5> */}
                            </div>
                            <div className="md:grid md:grid-cols-2">
                                <div>
                                    <h5 className="text-gray-400 font-bold uppercase text-xs">Pemasukan Bulan ini</h5>
                                    <NumberFormat thousandSeparator='.' decimalSeparator="," prefix="Rp. " displayType="text" value={paidThisMonth} className="text-2xl font-black text-green-500" />
                                </div>
                                <div>
                                    <h5 className="text-gray-400 font-bold uppercase text-xs">Piutang</h5>
                                    <NumberFormat thousandSeparator='.' decimalSeparator="," prefix="Rp. " displayType="text" value={unpaidTotal} className="text-2xl font-black text-red-600" />
                                </div>
                            </div>
                        </div>
                        <div className="bg-white rounded shadow p-5 mb-6">
                            <h5 className="text-xl font-bold mb-4">Pelanggan</h5>
                            <h5 className="text-6xl font-bold mb-4">{ countCustomer }</h5>
                            <h5 className="">{ countCustomerTransaction } transaksi pelanggan bulan ini</h5>
                        </div>
                        <div className="bg-white rounded shadow p-5 mb-5">
                            <h5 className="text-xl font-bold">Faktur</h5>
                            <Doughnut data={dataDoughnut} options={optionDoughnut} />
                        </div>
                    </div>
                </div>
            </Container>
        </Dashboard>
    )
}


export default DashoardIndex