import Image from 'next/image'
import { faFolder, faRightToBracket } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { Formik } from "formik"
import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react"
import Alert from "../components/atoms/alert"
import Button from "../components/atoms/button"
import Container from "../components/atoms/container"
import FormError from "../components/atoms/input/formError"
import FormInput from "../components/atoms/input/formInput"
import FormLabel from "../components/atoms/input/formLabel"
import Slink from "../components/atoms/links/link"
import Header from "../components/molecules/header"
import FrontLayout from "../components/templates/front"
import { Context } from "../contexts/store"
import { backendUrl } from "../helpers/constants"
import { JsonResponse } from "../helpers/type"
import useScript from "../hooks/useScripts"

const RegisterPage = () => {
    const { globalState, dispatch } = useContext(Context);
    const router = useRouter()
    const [errorRegister, setErrorRegister] = useState('')
    const [showErrorRegister, setShowErrorRegister] = useState(false)

    const normalRegister = async (name: string, username: string, email: string, no_telepon: string, password: string) => {
        try {
            const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + '/auth/register', {
                name,
                username,
                email,
                no_telepon,
                password
            })
            if (res.status === 200) {
                localStorage.setItem('token', res.data.data.token)
                dispatch({ type: "SET_USER", payload: res.data.data.user })

                router.push('/registration/create-store')
            }
        } catch (e: any) {
            if (e.response) {
                setErrorRegister(e.response.data.message)
                setShowErrorRegister(true)
            } else {
                alert(e)
            }
        }
    }

    useEffect(() => {
        if (globalState.auth.isUserAuthenticated) {
            router.push('/registration/create-store')
        }
    }, [globalState.auth.isUserAuthenticated, router])

    useScript("https://accounts.google.com/gsi/client")
    // useScript("/login.js")

    useEffect(() => {
        const script = document.createElement('script');

        script.text = `function parseJwt(e){var o=e.split(".")[1].replace(/-/g,"+").replace(/_/g,"/"),n=decodeURIComponent(atob(o).split("").map(function(e){return"%"+("00"+e.charCodeAt(0).toString(16)).slice(-2)}).join(""));return JSON.parse(n)}async function onSignIn(e){const o=parseJwt(e.credential);console.log("ID: "+o.sub),console.log("Full Name: "+o.name),console.log("Given Name: "+o.given_name),console.log("Family Name: "+o.family_name),console.log("Image URL: "+o.picture),console.log("Email: "+o.email);try{const e={method:"POST",headers:{Accept:"application/json","Content-Type":"application/json"},body:JSON.stringify({name:o.name,email:o.email,username:o.given_name})},n=await fetch("/api/auth/google",e),a=await n.json();localStorage.setItem("token",a.data.token),window.location.href="/auth/google"}catch(e){console.log(e)}}`;
        script.async = true;

        document.body.appendChild(script);

        return () => {
            document.body.removeChild(script);
        }
    }, []);


    return (
        <FrontLayout>
            <Header
                title="Buat Akun Storin"
                description="Kelola Produk dan Penjualanmu dari Satu Tempat dengan Mudah. Beralih ke pencatatan penjualan digital, buat invoice, kelola produk, dan lihat laporan bulanan lewat Storin."
            />
            <div className="grid lg:grid-cols-2">
                <div className="bg-blue-500 min-h-screen hidden lg:inline">
                    <Container className="pt-24 text-white flex items-center min-h-screen">
                        <div className="w-full text-center">
                            <div className="mb-5">
                                <Image src="/images/invoice.png" layout="responsive" height={406} width={696} alt="storin" />
                            </div>
                            <h5 className="text-lg">Buat faktur dan kelola penjualan bisnismu melalui storin.</h5>
                        </div>
                    </Container>
                </div>
                <div>
                    <Container className="pt-24">
                        <div className="xl:px-24">
                            <div className="text-center mb-5">
                                <FontAwesomeIcon icon={faFolder} className="text-5xl text-blue-500 mb-5" />
                                <h1 className="text-3xl font-bold">Halo!</h1>
                                <h5 className="text-gray-500 text-sm">Selamat datang di Storin, daftarkan diri untuk mendapatkan akun storin agar dapat mengakses semua fitur.</h5>
                            </div>
                            <Formik
                                initialValues={{ name: '', username: '', email: '', no_telepon: '', password: '', password_confirm: '' }}
                                validate={values => {
                                    const errors: any = {}
                                    if (!values.name) {
                                        errors.name = 'Nama harus diisi'
                                    }

                                    if (!values.email) {
                                        errors.email = 'Email harus diisi'
                                    } else if (
                                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                                    ) {
                                        errors.email = 'Email tidak valid';
                                    }

                                    if (!values.username) {
                                        errors.username = 'Username harus diisi'
                                    }

                                    if (!values.no_telepon) {
                                        errors.no_telepon = 'No. Telepon harus diisi'
                                    }

                                    if (!values.password) {
                                        errors.password = 'Password harus diisi'
                                    }

                                    if (values.password.length < 8) {
                                        errors.password = 'Panjang password minimal 8 karakter'
                                    }

                                    if (values.password_confirm != values.password) {
                                        errors.password_confirm = 'Password tidak sama'
                                    }

                                    return errors
                                }}
                                onSubmit={(values, { setSubmitting }) => {
                                    setSubmitting(true)
                                    normalRegister(values.name, values.username, values.email, values.no_telepon, values.password).then(r => setSubmitting(false))
                                }}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                    isSubmitting,
                                }) => (
                                    <form onSubmit={handleSubmit}>
                                        <Alert variant="error" className="mb-5" show={showErrorRegister}>
                                            {errorRegister}
                                        </Alert>
                                        <div className="mb-5">
                                            <FormLabel htmlFor="nama">Nama Lengkap</FormLabel>
                                            <FormInput
                                                type={"text"}
                                                placeholder="Masukan Nama Lengkap"
                                                id="name"
                                                name="name"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.name}
                                            />
                                            <FormError>
                                                {errors.name && touched.name && errors.name}
                                            </FormError>
                                        </div>
                                        <div className="mb-5">
                                            <FormLabel htmlFor="username">Username</FormLabel>
                                            <FormInput
                                                type={"text"}
                                                placeholder="Buat Username"
                                                id="username"
                                                name="username"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.username}
                                            />
                                            <FormError>
                                                {errors.username && touched.username && errors.username}
                                            </FormError>
                                        </div>
                                        <div className="grid grid-cols-2 gap-x-2 mb-5">
                                            <div>
                                                <FormLabel htmlFor="email">Email</FormLabel>
                                                <FormInput
                                                    type={"email"}
                                                    placeholder="Masukan Email"
                                                    id="email"
                                                    name="email"
                                                    onBlur={handleBlur}
                                                    onChange={handleChange}
                                                    value={values.email}
                                                />
                                                <FormError>
                                                    {errors.email && touched.email && errors.email}
                                                </FormError>
                                            </div>
                                            <div>
                                                <FormLabel htmlFor="no_telepon">No Telepon</FormLabel>
                                                <FormInput
                                                    type={"text"}
                                                    placeholder="Masukan No Telepon"
                                                    id="no_telepon"
                                                    name="no_telepon"
                                                    onBlur={handleBlur}
                                                    onChange={handleChange}
                                                    value={values.no_telepon}
                                                />
                                                <FormError>
                                                    {errors.no_telepon && touched.no_telepon && errors.no_telepon}
                                                </FormError>
                                            </div>
                                        </div>
                                        <div className="mb-5">
                                            <FormLabel htmlFor="password">Password</FormLabel>
                                            <FormInput
                                                type={"password"}
                                                placeholder="Masukan Password"
                                                id="password"
                                                name="password"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.password}
                                            />
                                            <FormError>
                                                {errors.password && touched.password && errors.password}
                                            </FormError>
                                        </div>
                                        <div className="mb-5">
                                            <FormLabel htmlFor="password_confirm">Konfirmasi Password</FormLabel>
                                            <FormInput
                                                type={"password"}
                                                placeholder="Masukan Konfirmasi Password"
                                                id="password_confirm"
                                                name="password_confirm"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.password_confirm}
                                            />
                                            <FormError>
                                                {errors.password_confirm && touched.password_confirm && errors.password_confirm}
                                            </FormError>
                                        </div>
                                        <Button variant="primary" className="w-full py-2 mb-4" type="submit" disabled={isSubmitting}>
                                            {isSubmitting ? 'Memproses...' : 'Daftar'}
                                        </Button>
                                    </form>
                                )}
                            </Formik>
                            <p className="text-center mb-2">atau</p>
                            <div id="g_id_onload"
                                data-client_id="709110421014-de1jtcnbgnos0dkgl4gf5fhdhk27mvc0.apps.googleusercontent.com"
                                data-callback="onSignIn"
                            >
                            </div>
                            <div className="flex justify-center">
                                <div className="g_id_signin" data-type="standard" data-size="large" data-width="320"></div>
                            </div>
                            <div className="mt-10 mb-8">
                                <h5 className="text-center">
                                    <span className="mr-2">Sudah memiliki akun?</span>
                                    <Slink href="/login">masuk sekarang</Slink>
                                </h5>
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
        </FrontLayout>
    )
}

export default RegisterPage