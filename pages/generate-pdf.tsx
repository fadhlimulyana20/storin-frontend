import jsPDF from "jspdf"

export default function GeneratePDF() {
    const generate = () => {
        const doc = new jsPDF('l', 'mm', [1200, 1810])
        const html = document?.getElementById("text2pdf")?.innerHTML || ""

        doc.html(html, {
            callback: function(doc) {
                doc.save("test.pdf")
            }
        })
    }

    return (
        <div>
            <div id="text2pdf">
                <h1>Hello world</h1>
            </div>
            <button onClick={() => generate()}>Generate</button>
        </div>
    )
}