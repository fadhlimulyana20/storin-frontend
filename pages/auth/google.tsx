import axios, { AxiosResponse } from "axios"
import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react"
import toast from "react-hot-toast"
import DefaultLoadingScreen from "../../components/molecules/loadingScreen"
import { Context } from "../../contexts/store"
import { backendUrl } from "../../helpers/constants"
import { JsonResponse } from "../../helpers/type"

export default function GoogleAuthOnSuccess() {
    const [loading, setIsloading] = useState<boolean>(true)
    const {dispatch, globalState} = useContext(Context)
    const [userId, setUserId] = useState("")
    const router = useRouter()

    useEffect(() => {
        const token = localStorage.getItem('token')

        const fetchAuthenticatedUser = async () => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/auth/me', {
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                })

                if (res.status === 200) {
                    dispatch({ type: "SET_USER", payload: res.data.data.user })
                    setUserId(res.data.data.user.id)
                    // console.log(res.data)
                }
            } catch (e: any) {
                if (e.response) {
                    dispatch({ type: "LOGOUT", payload: null })
                    router.push("/login")
                    toast.error(e.response.data.message)
                } else {
                    alert(e)
                }
            }
        }

        if (token) {
            fetchAuthenticatedUser()
        }

    }, [dispatch, router])

    useEffect(() => {
        const fetchMyStore = async (userId: string) => {
            // const userId = globalState.auth.loggedUser.id
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/user/" + userId)

                if (res.status === 200) {
                    // console.log(res)
                    router.push("/dashboard")
                }
            } catch (e: any) {
                router.push("/registration/create-store")
            }
        }
        if (globalState.auth.isUserAuthenticated && userId) {
            fetchMyStore(userId)
        }
    }, [globalState.auth.isUserAuthenticated, router, userId])

    return (
        <div>
            { loading ? (
                <DefaultLoadingScreen />
            ) : "" }
        </div>
    )
}