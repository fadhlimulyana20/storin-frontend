import { faImage } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import Image from "next/image"
import Link from "next/link"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import toast from "react-hot-toast"
import NumberFormat from "react-number-format"
import Container from "../../../components/atoms/container"
import Header from "../../../components/molecules/header"
import FrontLayout from "../../../components/templates/front"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"

const StoreCatalogue = () => {
    const router = useRouter()
    const { nickname } = router.query
    const [store, setStore] = useState<any>(null)
    const [product, setProduct] = useState<Array<any>>([])

    useEffect(() => {
        const GetStoreDetail = async () => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/n/" + nickname)

                if (res.status === 200) {
                    setStore(res.data.data)
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    } else {
                        alert(e)
                    }
                } else {
                    alert(e)
                }
            }
        }

        if (nickname) {
            GetStoreDetail()
        }
    }, [nickname])

    useEffect(() => {
        const GetStoreProduct = async () => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + '/product/user/' + store?.id)

                if (res.status === 200) {
                    setProduct(res.data.data)
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    } else {
                        alert(e)
                    }
                } else {
                    alert(e)
                }
            }
        }

        if (store) {
            GetStoreProduct()
        }
    }, [store])


    return (
        <FrontLayout>
            <Header
                title={typeof nickname === "string" ? nickname : ''}
                description="Produk"
            />
            <Container className="pt-24">
                <div className="flex gap-x-2 items-center">
                    <div className="border rounded h-20 w-20 flex items-center justify-center">
                        {store?.path_logo ? (
                            <div className="rounded w-full">
                                <Image src={store.path_logo} alt='preview' layout='responsive' objectFit="contain" height={250} width={250} className="hover:opacity-50" />
                            </div>
                        ) : (
                            <FontAwesomeIcon icon={faImage} className='text-gray-500' />
                        )}
                    </div>
                    <div>
                        <h1 className="text-2xl font-bold">{store ? store.name : ''}</h1>
                        <h5>@{nickname}</h5>
                    </div>
                </div>

                <div className="grid md:grid-cols-4 gap-4 py-4">
                    {product.map(p => {
                        return (
                            <Link href={`/s/product/${p?.id}`} key={p?.id}>
                                <a className="bg-white shadow-sm rounded p-3 border hover:opacity-70 hover:cursor-pointer">
                                    <div className="flex items-center h-max mb-4">
                                        {p?.path_image ? (
                                            <div className="rounded-lg overflow-hidden w-full">
                                                <Image src={p?.path_image} alt='preview' layout='responsive' objectFit="contain" height={250} width={250} />
                                            </div>
                                        ) : (
                                            <div className="rounded-lg overflow-hidden w-full">
                                                <Image src="/images/product-placeholder.jpg" alt='preview' layout='responsive' objectFit="contain" height={250} width={250} />
                                            </div>
                                        )}
                                    </div>
                                    <h5 className="font-bold">{p?.name}</h5>
                                    <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={p?.price} className="text-sm" />
                                </a>
                            </Link>
                        )
                    })}
                </div>
            </Container>
        </FrontLayout>
    )
}

export default StoreCatalogue