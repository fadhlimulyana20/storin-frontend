import { faImage } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios, { AxiosResponse } from "axios"
import { GetServerSideProps } from "next"
import Image from "next/image"
import Link from "next/link"
import { useEffect, useState } from "react"
import toast from "react-hot-toast"
import NumberFormat from "react-number-format"
import Container from "../../../components/atoms/container"
import FrontLayout from "../../../components/templates/front"
import { backendUrl } from "../../../helpers/constants"
import { JsonResponse } from "../../../helpers/type"

const ProductDetail = ({ data }: any) => {
    const [store, setStore] = useState<any>()

    useEffect(() => {
        const GetStoreDetail = async () => {
            try {
                const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/store/" + data?.store_id)

                if (res.status === 200) {
                    setStore(res.data.data)
                }
            } catch (e: any) {
                if (e.response) {
                    if (e.response.data) {
                        toast.error(e.response.data.message)
                    } else {
                        alert(e)
                    }
                } else {
                    alert(e)
                }
            }
        }
        
        GetStoreDetail()

    }, [data?.store_id])

    return (
        <FrontLayout>
            <Container>
                <div className="grid md:grid-cols-2 pt-24 gap-10">
                    <div className="h-50 w-50">
                        {data?.path_image ? (
                            <div className="rounded-lg overflow-hidden">
                                <Image src={data?.path_image} alt='preview' layout='responsive' objectFit="contain" height={250} width={250} />
                            </div>
                        ) : (
                            <div className="rounded overflow-hidden">
                                <Image src="/images/product-placeholder.jpg" alt='preview' layout='responsive' objectFit="contain" height={250} width={250} />
                            </div>
                        )}
                    </div>
                    <div>
                        <div className="mb-5">
                            <h1 className="text-4xl font-bold">{data?.name}</h1>
                            <NumberFormat thousandSeparator='.' decimalSeparator="," prefix='Rp. ' displayType="text" value={data?.price} className="text-2xl" />
                        </div>
                        <div className="mb-5">
                            <p>{data?.description}</p>
                        </div>
                        <div className="border-t flex gap-x-2 py-2">
                            <div className="border rounded h-12 w-12 flex items-center justify-center">
                                {store?.path_logo ? (
                                    <div className="rounded w-full">
                                        <Image src={store.path_logo} alt='preview' layout='responsive' objectFit="contain" height={250} width={250} className="hover:opacity-50" />
                                    </div>
                                ) : (
                                    <FontAwesomeIcon icon={faImage} className='text-gray-500' />
                                )}
                            </div>
                            <div>
                                <Link href={`/s/${store?.nickname}`}>
                                    <a className="font-bold mb-0 hover:text-blue-500">
                                        {store ? store.name : ''}
                                    </a>
                                </Link>
                                <h5 className="text-sm">@{store?.nickname}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </FrontLayout>
    )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const { id } = context.query

    // Fetch data from external API
    const res: AxiosResponse<JsonResponse> = await axios.get(backendUrl + "/product/" + id)
    const { data } = res.data

    // Pass data to the page via props
    return { props: { data } }
}

export default ProductDetail