import { faInstagram, faTwitter } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Head from "next/head"
import Image from "next/image"
import { useRouter } from "next/router"
import Button from "../components/atoms/button"
import Container from "../components/atoms/container"
import OpenGraph from "../components/atoms/openGraph"
import Header from "../components/molecules/header"
import FrontLayout from "../components/templates/front"

const Home = () => {
  const router = useRouter()

  return (
    <FrontLayout>
      <Header 
        title="Storin.id - Buat dan Kelola Invoice secara Digital"
        description="Kelola Produk dan Penjualanmu dari Satu Tempat dengan Mudah. Beralih ke pencatatan penjualan digital, buat invoice, kelola produk, dan lihat laporan bulanan lewat Storin."
      />
      <div className="min-h-screen flex items-center pt-20 bg-blue-500">
        <Container>
          <div className="grid md:grid-cols-2 md:gap-x-4 gap-5 flex items-center">
            <div className="text-center md:hidden inline">
              <Image src="/images/storin_front.png" layout="responsive" height={400} width={600} alt="storin" />
            </div>
            <div>
              <div className="mb-5 text-white">
                <h1 className="text-3xl font-black">Kelola Produk dan Penjualanmu dari Satu Tempat dengan Mudah</h1>
                <p className="text-lg">Beralih ke pencatatan penjualan digital, buat invoice, kelola produk, dan lihat laporan bulanan lewat Storin.</p>
              </div>
              <Button variant="outline-primary" className="py-3 px-5 font-bold" onClick={() => router.push('/register')}>
                Mulai Secara Gratis
              </Button>
            </div>
            <div className="text-center md:inline hidden">
              <Image src="/images/storin_front.png" layout="responsive" height={400} width={600} alt="storin" />
            </div>
          </div>
        </Container>
      </div>
      <div className="pt-20 mb-20">
        <Container>
          <div className="md:px-20">
            <div className="text-center mb-20">
              <h1 className="text-2xl font-bold">Kami menyediakan fitur</h1>
              <h1 className="text-2xl font-bold">untuk memudahkan pekerjaanmu</h1>
            </div>
            <div className="mb-10 grid md:grid-cols-2 flex items-center">
              <div className="w-1/2 mx-auto mb-5 md:mb-0">
                <Image src="/images/packages.png" layout="responsive" height={500} width={500} alt="produk" />
              </div>
              <div>
                <h1 className="text-xl font-bold">Kelola Produk</h1>
                <p className="text-lg">
                  Kamu memiliki banyak produk untuk dijual dan bingung
                  mengingat-ingat harga dan stoknya. Storin menyediakan
                  tempat untuk mengelola produk dengan mudah. Kamu
                  tidak perlu menghitung ulang stok setiap kali ada penjualan
                  karena semua tercatat secara otomatis.
                </p>
              </div>
            </div>
            <div className="mb-10 grid md:grid-cols-2 flex items-center">
              <div className="w-1/2 mx-auto mb-5 md:mb-0 inline md:hidden">
                <Image src="/images/bill.png" layout="responsive" height={500} width={500} alt="produk" />
              </div>
              <div>
                <h1 className="text-xl font-bold">Kelola Penjualan</h1>
                <p className="text-lg">
                  Kamu memiliki banyak konsumen dan tidak sempat
                  mencatat penjualan atau memberikan nota kepada
                  pelanggan. Dengan storin kamu bisa melaukannya
                  tanpa harus menulis. Semua tercatat secara otomatis.
                </p>
              </div>
              <div className="w-1/2 mx-auto mb-5 md:mb-0 md:inline hidden">
                <Image src="/images/bill.png" layout="responsive" height={500} width={500} alt="produk" />
              </div>
            </div>
            <div className="mb-10 grid md:grid-cols-2 flex items-center">
              <div className="w-1/2 mx-auto mb-5 md:mb-0">
                <Image src="/images/analytics.png" layout="responsive" height={500} width={500} alt="produk" />
              </div>
              <div>
                <h1 className="text-xl font-bold">Dapatkan Laporan</h1>
                <p className="text-lg">
                  Kamu tidak perlu repot-repot lagi untuk membuat laporan
                  keungan bisnis mu setiap bulannya. Dengan storin kamu
                  hanya cukup melihatnya melalui aplikasi dan bisa diexpor
                  untuk memudahkan penyimpanan dokumen.
                </p>
              </div>
            </div>
          </div>
        </Container>
      </div>

      <div className="py-20 bg-blue-500 mb-10">
        <Container>
          <div className="grid md:grid-cols-2 gap-4 flex items-center">
            <div className="px-5">
              <iframe className="w-full h-72 rounded" src="https://www.youtube.com/embed/w1TBQQid7oE" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
            <div className="text-white">
              <h1 className="text-2xl font-bold mb-4">Bagaimana Storin Bekerja</h1>
              <p className="text-lg">Kamu bisa menggunakan storin untuk bisnis apapun. Kamu bisa menambahkan produk serta mengelola stok. Kamu bisa membuat nota dan invoice penjualan dari produk yang kamu jual. Pada setiap periode kamu bisa mengecek laporan keuangan untuk mengevaluasi kinerja bisnis kamu.</p>
            </div>
          </div>
        </Container>
      </div>

      <div className="py-10">
        <Container>
          <div className="bg-blue-500 px-3 py-10 rounded-lg">
            <h1 className="text-center text-white text-2xl font-bold mb-5">Kelola bisnis mu menggunakan Storin sekarang.</h1>
            <div className="flex justify-center">
              <button className="px-5 py-3 rounded bg-white hover:bg-gray-50 text-blue-500 font-bold">Mulai Secara Gratis</button>
            </div>
          </div>
        </Container>
      </div>

    </FrontLayout>
  )
}

export default Home