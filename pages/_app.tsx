import '../styles/globals.scss'
import type { AppProps } from 'next/app'
import Store, { Context } from '../contexts/store'
import toast, { Toaster } from 'react-hot-toast'
import { useContext, useEffect } from 'react'
import axios, { AxiosResponse } from 'axios'
import { JsonResponse } from '../helpers/type'
import { backendUrl } from '../helpers/constants'
import Script from 'next/script'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Store>
      <Script id="google-tag-manager" strategy="afterInteractive">
        {`
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MCCJLKR');
      `}
      </Script>
      <Toaster />
      <Component {...pageProps} />
    </Store>
  )
}

export default MyApp
