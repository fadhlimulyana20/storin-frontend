import Image from "next/image";
import Container from "../components/atoms/container";
import Header from "../components/molecules/header";
import FrontLayout from "../components/templates/front";

export default function AboutUs() {
    return (
        <FrontLayout>
            <Header
                title="Tentang Storin"
                description="Kami menyediakan layanan perangkat pembantu invoicing dan akuntansi untuk memudahkan pekerjaan para pebisnis."
            />
            <div className="bg-blue-500 min-h-screen">
                <div className="min-h-screen flex items-center z-10 absolute">
                    <div className="grid md:grid-cols-2 w-full pt-10">
                        <Container>
                            <div className="bg-white rounded-lg text-blue-500 p-8">
                                <h2 className="text-4xl font-black mb-2">Tentang Storin</h2>
                                <p className="text-2xl font-medium">Kami menyediakan layanan perangkat pembantu invoicing dan akuntansi untuk memudahkan pekerjaan para pebisnis.</p>
                            </div>
                        </Container>
                    </div>
                </div>
                <div className="md:inline hidden">
                    <Image src="/images/doodle-about-us-wide_lite.png" layout="fill" alt="storin" objectFit="cover" className="md:hidden inline" />
                </div>
                <div className="md:hidden inline">
                    <Image src="/images/doodle-about-us-lite.png" layout="fill" objectFit="cover" alt="storin" className="md:hidden inline" />
                </div>
            </div>
        </FrontLayout>
    )
}