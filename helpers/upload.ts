import axios, { AxiosResponse } from "axios"
import { backendUrl } from "./constants"
import { JsonResponse } from "./type"

export const uploadGlobal = async (file: File, path: string, token: string) => {
    try {
        let formData = new FormData()
        formData.append('file', file)
        formData.append('file_path', path)

        const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + "/upload/global", formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })

        if (res.status == 200) {
            return (res.data)
        }
    } catch(e: any) {
        return e
    }
}

export const getGlobal = async (path: string, token: string) => {
    try {
        const res: AxiosResponse<JsonResponse> = await axios.post(backendUrl + "/upload/get", {
            file_path: path,
        }, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })

        if (res.status == 200) {
            return (res.data)
        }
    } catch(e: any) {
        return e
    }
}