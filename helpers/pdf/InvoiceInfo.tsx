import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const InvoiceInfoContainer = styled.table`
  font-size: 0.875em;
  td {
    padding: 4px 0;
  }
`

const ClientName = styled.td<any>`
  font-size: 1.5em;
  vertical-align: top;
`

const InvoiceInfo = ({
  invoiceNumber,
  invoiceDate,
  clientName,
  companyName,
  companyEmail,
  addressStreet,
  addressCityStateZip,
}: InvoiceInfoProps) => (
  <InvoiceInfoContainer>
    <tr>
      <ClientName rowspan="2">
        {clientName}
      </ClientName>
      <td>
        {companyName}
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        {addressStreet}
      </td>
    </tr>
    <tr>
      <td>
        Invoice Date: <strong>{invoiceDate}</strong>
      </td>
      <td>
        {addressCityStateZip}
      </td>
    </tr>
    <tr>
      <td>
        Invoice No: <strong>{invoiceNumber}</strong>
      </td>
      <td>
        {companyEmail}
      </td>
    </tr>
  </InvoiceInfoContainer>
)

interface InvoiceInfoProps {
  invoiceNumber: string
  invoiceDate: string
  clientName: string
  companyName: string
  companyEmail: string
  addressStreet: string
  addressCityStateZip: string
}

export default InvoiceInfo