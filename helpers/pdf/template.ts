export const PdfTemplate = ({ name, no_invoice_string, due_date, issued_date, items, customer, store, config }: any) => {
    const today = new Date();
    return `
    <!doctype html>
    <html>
        <head>
            <meta charset="utf-8" />
            <title>A simple, clean, and responsive HTML invoice template</title>
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">            <style id="table_style">
                @page { size: auto;  margin: 0mm; }
                .bold{
                    font-weight:bold;
                }

                .button {
                    background-color: #276bd8 !important;
                    padding: 0.5em 1em 0.5em 1em;
                    font-weight: 700;
                    color: #ffff;
                    border-radius: 0.25em;
                    border: none;
                    cursor: pointer;
                }

                .button:hover {
                    background-color: #273ed8 !important;
                }

                tr.note table td {
                    padding-top:20px;
                }

                .note .note-text {
                    font-size:12px;
                }

                .note ul {
                    padding-top: 0;
                    padding-left: 15px;
                }

                .note ul li{
                    margin-bottom:0;
                    padding:0;
                }
                
                .container {
                    max-width: 800px;
                    margin-right: auto;
                    margin-left: auto;
                }
                
                .invoice-box {
                    max-width: 800px;
                    margin-right: auto;
                    margin-left: auto;
                    padding: 30px;
                    /* border: 1px solid #eee; */
                    /* box-shadow: 0 0 10px rgba(0, 0, 0, 0.15); */
                    /* border-radius:10px; */
                    font-size: 12px;
                    line-height: 20px;
                    /* font-family: Arial, sans-serif; */
                    font-family: 'Lato', sans-serif;
                    color: #555;
                }

                .invoice-box table {
                    width: 100%;
                    line-height: inherit;
                    text-align: left;
                }

                .invoice-box table td {
                    padding: 5px;
                    vertical-align: middle;
                }

                .invoice-box table tr td.right {
                    text-align: right;
                }

                .invoice-box table tr.top table td {
                    padding-bottom: 10px;
                }

                .invoice-box table tr.top table td.title {
                    font-size: 45px;
                    line-height: 45px;
                    color: #333;
                }

                .invoice-box table tr.information table td {
                    padding-bottom: 40px;
                }

                .invoice-box table tr.heading td {
                    background: #eee;
                    print-color-adjust: exact;
                    border-bottom: 1px solid #ddd;
                    font-weight: bold;
                }

                @media print {
                    .invoice-box table tr td {
                        font-size: 12px;
                    }

                    .invoice-box table tr.heading td {
                        background: #eee;
                        print-color-adjust: exact;
                        -webkit-print-color-adjust: exact;
                        border-bottom: 1px solid #ddd;
                        font-weight: bold;
                    }
                }

                .invoice-box table tr.details td {
                    padding-bottom: 20px;
                }

                .invoice-box table tr.item td {
                    border-bottom: 1px solid #eee;
                }

                .invoice-box table tr.item.last td {
                    border-bottom: none;
                }

                .invoice-box table tr.total td.total-data {
                    border-top: 2px solid #eee;
                    font-weight: bold;
                }

                @media only screen and (max-width: 600px) {
                    .invoice-box table tr.top table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }

                    .invoice-box table tr.information table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                }

                /** RTL **/
                .invoice-box.rtl {
                    direction: rtl;
                    /* font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; */
                    font-family: 'Lato', sans-serif;
                }

                .invoice-box.rtl table {
                    text-align: right;
                }

                .invoice-box.rtl table tr td:nth-child(2) {
                    text-align: left;
                }
            </style>
        </head>

        <body>
            <div class="container">
                <button class="button" onclick="generate()">Download</button>
            </div>
            <div id="invoice-box-div">
                <div class="invoice-box">
                    <table cellpadding="0" cellspacing="0">
                        <tr class="top">
                            <td colspan="7">
                                <table>
                                    <tr>
                                        <td class="title">
                                            <img src="${store.path_logo}"
                                                style="width: 100%; max-width: 70px" />
                                        </td>

                                        <td class="right">
                                            <span class="bold">Invoice #: ${no_invoice_string}</span><br />
                                            Diterbitkan: ${issued_date}<br />
                                            Jatuh Tempo: ${due_date}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr class="information">
                            <td colspan="7">
                                <table>
                                    <tr>
                                        <td>
                                            <span>Informasi Perusahaan:</span><br />
                                            <span class="bold">${store.name}</span><br />
                                        </td>

                                        <td class="right">
                                            <span>Tagihan Untuk:</span><br />
                                            <span class="bold">${customer.name}</span><br />
                                            ${customer.email}<br />
                                            ${customer.phone_number}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!--
                        <tr class="heading">
                            <td>Payment Method</td>

                            <td>Check #</td>
                        </tr>

                        <tr class="details">
                            <td>Check</td>

                            <td>1000</td>
                        </tr>
                        -->

                        <tr class="heading">
                            <td>SKU</td>
                            <td>Barang</td>
                            <td>Harga</td>
                            <td>Diskon</td>
                            <td>Pajak</td>
                            <td>Jumlah</td>
                            <td class="right">Total</td>
                        </tr>

                        ${items.map((i: any) => (`
                        <tr class="item">
                            <td>${i.sku}</td>
                            <td>${i.name}</td>
                            <td>${new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(i.price)}</td>
                            <td>${new Intl.NumberFormat('id-ID', { style: 'percent' }).format(i.discount)}</td>
                            <td>${new Intl.NumberFormat('id-ID', { style: 'percent' }).format(i.tax)}</td>
                            <td>${i.qty}</td>
                            <td class="right">${new Intl.NumberFormat('id-ID', {  style: 'currency', currency: 'IDR' }).format(i.total)}</td>
                        </tr>
                        `)).join('')}

                        <tr class="total">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="2">
                                Sub Total<br />
                                Diskon<br />
                                Pajak<br />
                                Total
                            </td>

                            <td class="total-data right">
                                ${new Intl.NumberFormat('id-ID', {  style: 'currency', currency: 'IDR' }).format(items.map((i: any) => i.total).reduce((prev: any, curr: any) => prev + curr, 0))}<br />
                                ${new Intl.NumberFormat('id-ID', {  style: 'currency', currency: 'IDR' }).format(items.map((p: any) => (p.qty * p.price) * ((parseFloat(p.discount)) / 100)).reduce((prev: any, curr: any) => prev + curr, 0))}<br />
                                ${new Intl.NumberFormat('id-ID', {  style: 'currency', currency: 'IDR' }).format(items.map((p: any) => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((parseFloat(p.tax)) / 100)).reduce((prev: any, curr: any) => prev + curr, 0))}<br />
                                ${new Intl.NumberFormat('id-ID', {  style: 'currency', currency: 'IDR' }).format(items.map((p: any) => (p.qty * p.price) * ((100 - parseFloat(p.discount)) / 100) * ((100 + parseFloat(p.tax)) / 100)).reduce((prev: any, curr: any) => prev + curr, 0))}
                            </td>
                        </tr>
                        <tr class="note">
                            <td colspan="3">
                                <table>
                                    <tr>
                                        <td>
                                            <span class="bold">Catatan:</span><br />
                                            <span class="note-text">${config.note}</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.umd.min.js"></script>-->
            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/2.3.8/purify.min.js" integrity="sha512-M72KfQy4kPuLYC6CeTrN0eA17U1lXEMrr5qEJC/40CLdZGC3HpwPS0esQLqBHnxty2FIcuNdP9EqwSOCLEVJXQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
            <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js" integrity="sha512-BNaRQnYJYiPSqHHDb58B0yaPfCu+Wgds8Gp/gU33kqBtgNS4tSPHuGibyoeqMV/TJlSKda6FXzoEyYGjTe+vXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
            <script>
                // window.jsPDF = window.jspdf.jsPDF
                // window.html2canvas = window.html2canvas
                function generate() {
                    // var doc = new jsPDF('p', 'mm', [297, 210]);
                    // const html = document?.getElementById("invoice-box-div") || ""
                    // html2canvas(html).then((canvas) => {
                    //     console.log(canvas)
                    //     const img = canvas.toDataURL('image/png')
                    //     doc.addImage(img, 'PNG', 10, 10)
                    //     doc.save("test.pdf")
                    // })
            
                    // doc.html(html[0], {
                    //     callback: function(doc) {
                    //         doc.save("test.pdf")
                    //     }
                    // })
                    var divContents = document?.getElementById("invoice-box-div")?.innerHTML || ""
                    var printWindow = window.open('', '', 'height=1123,width=794');
                    printWindow.document.write('<html><head>');
                    printWindow.document.write('<style type = "text/css">');
                    var table_style = document.getElementById("table_style").innerHTML;
                    printWindow.document.write(table_style);
                    printWindow.document.write('</style>');
                    printWindow.document.write('</head><body >');
                    printWindow.document.write(divContents);
                    printWindow.document.write('</body></html>');
                    printWindow.document.close();
                    printWindow.print()
                }
            </script>
        </body>
    </html>
    `;
};