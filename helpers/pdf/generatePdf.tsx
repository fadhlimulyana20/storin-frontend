import Invoice from './invoice'
import { ServerStyleSheet } from 'styled-components'
import ReactDOMServer from 'react-dom/server'

export function buildHTMLToPDFPayload() {
    const sheet = new ServerStyleSheet()
    const html = ReactDOMServer.renderToStaticMarkup(
        sheet.collectStyles(
            <Invoice />
        )
    )

    return html
}