export const convertISOtoDateTimeForm = (date: Date) => {
    const options: Intl.DateTimeFormatOptions = { timeZone: "Asia/Jakarta", hour12: false, timeStyle:"short" };
    const tanggal_time = date.toLocaleTimeString("en-US", options)
    const bulan = () => {
        if (date.getMonth() < 9) {
            return "0"+(date.getMonth()+1)
        } else {
            return String(date.getMonth() + 1)
        }
    }

    const hari = () => {
        if (date.getDate() < 10) {
            return "0" + (date.getDate())
        }else {
            return String(date.getDate())
        }
    }

    const date_only_string = date.getFullYear() + "-" + bulan() + "-" + hari()
    const date_time_form_string = date_only_string + "T" + tanggal_time
    return date_time_form_string
}

export const convertISOtoDateForm = (date: Date) => {
    const options: Intl.DateTimeFormatOptions = { timeZone: "Asia/Jakarta", hour12: false, timeStyle:"short" };
    const tanggal_time = date.toLocaleTimeString("en-US", options)
    const bulan = () => {
        if (date.getMonth() < 9) {
            return "0"+(date.getMonth()+1)
        } else {
            return String(date.getMonth() + 1)
        }
    }

    const hari = () => {
        if (date.getDate() < 10) {
            return "0" + (date.getDate())
        }else {
            return String(date.getDate())
        }
    }

    return date.getFullYear() + "-" + bulan() + "-" + hari()
}

const dString = (d: number) => {
    if (d < 10) {
        return "0" + d
    } 

    return d.toString()
}

const monthString = (m: number) => {
    const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    return months[m]
}

export const dateRangeString = (dateStart: Date, dateEnd: Date) => {
    const yearStart = dateStart.getFullYear()
    const yearEnd = dateEnd.getFullYear()
    const isYearSame = yearStart === yearEnd

    const monthStart = dateStart.getMonth()
    const monthEnd = dateEnd.getMonth()
    const isMonthSame = monthStart === monthEnd

    const dStart = dateStart.getDate()
    // console.log(dStart)
    const dEnd = dateEnd.getDate()
    // console.log(dEnd)
    const isDateSame = dStart === dEnd

    if (isYearSame) {
        if (isMonthSame) {
            if (isDateSame) {
                return dString(dStart) + " " + monthString(monthStart) + " " + yearStart
            }

            return dString(dStart) + "-" + dString(dEnd) + " " + monthString(monthStart) + " " + yearStart
        }

        return dString(dStart) + " " + monthString(monthStart) + "-" + dString(dEnd) + " " + monthString(monthEnd) + " "  + yearStart
    }

    return dString(dStart) + " " + monthString(monthStart) + " " + yearStart + "-" + dString(dEnd) + " " + monthString(monthEnd) + " "  + yearEnd
}

export const dateString = (date: Date) => {
    const year = date.getFullYear()
    const month = date.getMonth()
    const d = date.getDate()

    return dString(d) + " " + monthString(month) + " " + year
}
