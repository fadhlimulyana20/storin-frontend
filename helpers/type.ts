export type JsonResponse = {
    status: string,
    message?: string,
    data?: any
} 
