import { Dispatch } from 'react'
import { AuthStateInterface } from './authReducerType';
import { CustomerStateInterface } from './customerReducerType';
import { DashboardInterface } from './dashboardType';
import { myStoreInterface } from './myStoreReducerType';

export interface GlobalStateInterface {
    auth: AuthStateInterface,
    dashboard: DashboardInterface,
    myStore: myStoreInterface,
    customer: CustomerStateInterface
}

export type ActionType = {
    type: string;
    payload?: any;
};

export type ContextType = {
    globalState: GlobalStateInterface;
    dispatch: Dispatch<ActionType>;
};

export type User = {
    id?: string,
    name: string,
    email: string,
    password: string,
    created_at: string,
    updated_at: string,
    deleted_at?: string
};