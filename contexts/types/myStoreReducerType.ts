export interface storeInterface {
    id: string
	name: string
	nickname: string
	path_logo: string
	user_id: string
    created_at?: string
    updated_at?: string
    deleted_at?: string
	is_deleted?: boolean
}

export interface myStoreInterface {
    store: storeInterface | any
}