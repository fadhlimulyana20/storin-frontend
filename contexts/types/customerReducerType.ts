export interface CustomerInterface {
    id: string
    name: string
    phone_number: string
    email: string
}

export interface CustomerStateInterface {
    customer: CustomerInterface | null
}