import { User } from ".";

export interface AuthStateInterface {
    isUserAuthenticated: boolean;
    loggedUser: User | any;
    persistenceType: string;
    roles: Array<any> | null;
}