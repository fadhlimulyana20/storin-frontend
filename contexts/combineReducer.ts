import { ActionType, GlobalStateInterface } from "./types";

type ContextReducers = {
    state: any;
    action: (state: GlobalStateInterface, action: ActionType) => GlobalStateInterface;
}

export const combineReducers = (reducers: any) => {
    return (state: any, action: any) => {
      return Object.keys(reducers).reduce(
        (acc, prop) => {
          return ({
            ...acc,
            ...reducers[prop]({ [prop]: acc[prop] }, action),
          })
        },
        state
      )
    }
  }