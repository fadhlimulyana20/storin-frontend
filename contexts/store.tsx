import React, {createContext, useReducer} from "react";
import { combineReducers } from "./combineReducer";
import authReducer from "./reducers/authReducer";
import customerReducer from "./reducers/customerReducer";
import dashboardReducer from "./reducers/dashboardReducer";
import myStoreReducer from "./reducers/myStoreReducer";
import { ContextType, GlobalStateInterface } from "./types";


const initialState: GlobalStateInterface = {
    auth: {
        isUserAuthenticated: false,
        loggedUser: null,
        persistenceType: "sessionStorage",
        roles: []
    },
    dashboard: {
        isSidebarHidden: true
    },
    myStore: {
        store: null
    },
    customer: {
        customer: null
    }
};

const reducers = combineReducers({
    auth: authReducer,
    dashboard: dashboardReducer,
    myStore: myStoreReducer,
    customer: customerReducer
})

const Store = ({children}: { children: React.ReactNode }): React.ReactElement => {
    const [globalState, dispatch] = useReducer(reducers, initialState);
    return (
        <Context.Provider value={{globalState, dispatch}}>
            {children}
        </Context.Provider>
    )
};

export const Context = createContext({} as ContextType);
Context.displayName = "Custom Reducer"
export default Store;