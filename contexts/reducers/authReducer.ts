import { ActionType, GlobalStateInterface } from "../types";

const authReducer = (state: GlobalStateInterface, action: ActionType) => {
    switch (action.type) {
        case 'SET_USER':
            return {
                ...state,
                auth: {
                    ...state.auth,
                    loggedUser: action.payload,
                    isUserAuthenticated: true,
                }
            }
        case 'LOAD_ROLES':
            return {
                ...state, 
                auth: {
                    ...state.auth,
                    roles: action.payload
                }
            }
        case 'LOGOUT':
            return {
                ...state,
                auth: {
                    ...state.auth,
                    loggedUser: null,
                    isUserAuthenticated: false,
                }
            }
        default:
            return state;
    }
};

export default authReducer;