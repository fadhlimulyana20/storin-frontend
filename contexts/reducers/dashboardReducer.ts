import { ActionType, GlobalStateInterface } from "../types";

const dashboardReducer = (state: GlobalStateInterface, action: ActionType) => {
    switch (action.type) {
        case 'TOGGLE_SIDEBAR':
            return {
                ...state,
                dashboard: {
                    ...state.dashboard,
                    isSidebarHidden: !state.dashboard.isSidebarHidden
                }
            }
        default:
            return state;
    }
};  

export default dashboardReducer;