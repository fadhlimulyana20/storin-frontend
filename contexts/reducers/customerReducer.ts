import { ActionType, GlobalStateInterface } from "../types";

const customerReducer = (state: GlobalStateInterface, action: ActionType) => {
    switch (action.type) {
        case 'SET_CUSTOMER':
            return {
                ...state,
                customer: {
                    ...state.customer,
                    customer: action.payload
                }
            }
        case 'UNSSET_CUSTOMER':
            return {
                ...state,
                customer: {
                    ...state.customer,
                    customer: null
                }
            }
        default:
            return state;
    }
};

export default customerReducer;