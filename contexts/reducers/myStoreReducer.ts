import { ActionType, GlobalStateInterface } from "../types";

const myStoreReducer = (state: GlobalStateInterface, action: ActionType) => {
    switch (action.type) {
        case 'SET_MY_STORE':
            return {
                ...state,
                myStore: {
                    ...state.myStore,
                    store: action.payload
                }
            }
        default:
            return state;
    }
};  

export default myStoreReducer;