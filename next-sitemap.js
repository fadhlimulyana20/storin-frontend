const siteUrl = 'https://www.storin.id'

module.exports = {
    siteUrl,
    generateRobotsTxt: true,
    robotsTxtOptions: {
        additionalSitemaps : [`${siteUrl}/sitemap.xml`],
    },
    exclude: ['/dashboard', '/dashboard/*'],
    sitemapSize: 7000,
};